```shell
#: 注释
$: 可执行命令
其他: 终端输出
```

### 一、创建一个pod实践

<img src="./assets/deployment.png" width=640 />

##### 命令行创建

创建一个POD

```shell
$ kubectl run kdemo-pod --image=registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5 --port=80
```

创建一个Deployment

```shell
$ kubectl create deployment kdemo-0810 --image=registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5 --port=80
```

如果需要自动化处理复杂的Kubernetes任务，常常需要编写Yaml配置文件。

##### Yaml文件创建

通过--dry-run参数打印一个deployment对象，不会执行命令

none: 默认值

server: 提交到服务端但是不持久化资源

client: 只打印对象，不发送到服务端

```shell
$ kubectl create deployment kdemo-0810 --image=registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5 --port=80 --dry-run=client -o yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: kdemo-0810
  name: kdemo-0810
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kdemo-0810
  strategy: { }
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: kdemo-0810
    spec:
      containers:
        - image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
          name: kdemo
          ports:
            - containerPort: 80
          resources: { }
status: { }
```

```shell
$ kubectl expose deployment kdemo-0810 --port=80 --type=NodePort --target-port=80 --name=kdemo-0810 --dry-run=client -o yaml
```

```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: kdemo-0810
  name: kdemo-0810
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: kdemo-0810
  type: NodePort
status:
  loadBalancer: {}
```

我们可以把deployment和service合并到一个.yaml文件，使用`---`分割

`kdemo-0810.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: kdemo-0810
  name: kdemo-0810
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kdemo-0810
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: kdemo-0810
    spec:
      containers:
      - image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
        name: kdemo
        ports:
        - containerPort: 80
        resources: {}
status: {}
---
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: kdemo-0810
  name: kdemo-0810
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: kdemo-0810
  type: NodePort
status:
  loadBalancer: {}
```

部署这个yaml文件

```shell
$ kubectl apply -f kdemo-0810.yaml

deployment.apps/kdemo-0810 created
service/kdemo-0810 configured

$ kubectl get svc
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
kdemo-0810   NodePort    10.97.217.34   <none>        80:31652/TCP   173m
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP        4d
```

访问 http://[任意节点IP]:[NodePort]

<img src="./assets/pod-lifecycle.png" alt="image-20210810155858181" width=640 />

##### 在线格式化工具

<a href="https://verytoolz.com/yaml-formatter.html" target="_blank">https://verytoolz.com/yaml-formatter.html</a>

### 二、pod的原理、生命周期

##### Pod在kubernetes的5个阶段(Phase)状态

| 名称      | 描述                                                         |
| --------- | ------------------------------------------------------------ |
| Pending   | 系统已经接受pod实例的创建，但其中所包含容器的一个或者多个image还没有创建成功。Pending包含调度计算与通过网络创建image，所以此phase的时间可能会有点长。 |
| Running   | Pod已经被调度到某个node上，pod包含的所有容器已经创建完成，至少有一个容器正常运行或者处于启动与重启动过程。 |
| Succeeded | Pod中的所有容器正常终止，并且不会再次启动。                  |
| Failed    | Pod中所有容器已终止运行，至少有一个容器非正常结束，比如退出码非0，被系统强制杀死等。 |
| Unknow    | 无法取得pod状态。                                            |

##### Pod周期的过程如下5步：

1.初始化容器阶段初始化pod中每一个容器,他们是串行执行的，执行完成后就退出了

2.启动主容器main container

3.在main container刚刚启动之后可以执行post start命令

4.在整个main container执行的过程中可以做两类探测：liveness probe(存活探测)和readiness probe(就绪探测)

5.在main container结束前可以执行pre stop命令

<img src="https://confluence.mysre.cn/download/attachments/46508641/399a17b0250db8c12d13524844c5eda21603447628050.png?version=1&modificationDate=1628588666000&api=v2" alt="生命周期" width=640 />

### 三、pod及事件处理函数

当容器启动以后Kubernetes会发送一个postStart事件，并且在容器退出之前理解发送一个preStop事件。

这两个钩子，我们也可以运用起来。

下面是这个Pod的配置文件：

lifecycle-demo.yaml

```shell
apiVersion: v1
kind: Pod
metadata:
  name: lifecycle-demo
spec:
  containers:
  - name: lifecycle-demo-container
    image: nginx
    lifecycle:
      postStart:
        exec:
          # 容器启动后往/usr/share/message写入Hello from the postStart handler
          command: ["/bin/sh", "-c", "echo kubernetes实践课-调度 > /root/message"]
      preStop:
        exec:
          # 容器终结前退出nginx
          command: ["/bin/sh","-c","/usr/sbin/nginx -s quit"]
```

```shell
# 部署
$ kubectl apply -f lifecycle-demo.yaml

# 查看是否执行了postStart
$ kubectl get pods
NAME                          READY   STATUS    RESTARTS   AGE
kdemo-0810-85fb4c758b-bjq44   1/1     Running   0          30m
lifecycle-demo                1/1     Running             0          23s

$ kubectl exec -it lifecycle-demo -- cat /root/message
kubernetes实践课-调度
```

### 四、init容器实践

Pod 可以包含多个容器，应用运行在这些容器里面，同时 Pod 也可以有一个或多个先于应用容器启动的 Init 容器。

Init 容器与普通的容器非常像，除了如下两点：

1、它们总是运行到完成。

2、每个都必须在下一个启动之前成功完成

同时 Init 容器不支持 Readiness Probe，因为它们必须在 Pod 就绪之前运行完成。

如果为一个 Pod 指定了多个 Init 容器，这些容器会按顺序逐个运行。每个 Init 容器必须运行成功，下一个才能够运行。

当所有的 Init 容器运行完成时，Kubernetes 才会为 Pod 初始化应用容器并像平常一样运行。

Init-pod1.yaml

```shell
apiVersion: v1
kind: Pod
metadata:
  name: init-pod1
  labels:
    app: init
spec:
  containers:
  - name: init-container
    image: busybox
    command: ['sh','-c','echo The app is running! & sleep 3600']
  initContainers:
  - name: init-nginx
    image: busybox
    # 等待nginx服务启动完成
    command: ['sh','-c','until nslookup nginx;do echo waiting for nginx;sleep 2;done;']
```

查看容器状态

```shell
$ kubectl describe pod init-pod1

...
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  33s   default-scheduler  Successfully assigned default/init-pod1 to vm-k8s-node-2
  Normal  Pulling    32s   kubelet            Pulling image "busybox"
  Normal  Pulled     15s   kubelet            Successfully pulled image "busybox" in 17.163951243s
  Normal  Created    15s   kubelet            Created container init-nginx
  Normal  Started    14s   kubelet            Started container init-nginx
```

```shell
# 创建nginx pod
$ kubectl run nginx --image=nginx --port=80

# 创建nginx service
$ kubectl expose pod nginx --port=80 --name=nginx

# 查看init状态，容器已经顺利启动
$ kubectl describe pod init-pod1
...
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  79s   default-scheduler  Successfully assigned default/init-pod1 to vm-k8s-node-2
  Normal  Pulling    78s   kubelet            Pulling image "busybox"
  Normal  Pulled     58s   kubelet            Successfully pulled image "busybox" in 19.771811018s
  Normal  Created    58s   kubelet            Created container init-nginx
  Normal  Started    58s   kubelet            Started container init-nginx
  Normal  Pulling    52s   kubelet            Pulling image "busybox"
  Normal  Pulled     29s   kubelet            Successfully pulled image "busybox" in 23.323857832s
  Normal  Created    28s   kubelet            Created container init-container
  Normal  Started    28s   kubelet            Started container init-container

```

### 五、pod探针

##### 探针类型

`livenessProbe`：存活检查

`readinessProbe`：就绪检查

##### 探测器

`ExecAction`：可执行脚本探测

`TCPSocketAction`：TCP探测

`HTTPGetAction`：HTTP 探测

##### 参数

`initialDelaySeconds`：容器启动后要等待多少秒后存活和就绪探测器才被初始化，默认是 0 秒，最小值是 0。

`periodSeconds`：执行探测的时间间隔（单位是秒）。默认是 10 秒。最小值是 1。

`timeoutSeconds`：探测的超时后等待多少秒。默认值是 1 秒。最小值是 1。

`successThreshold`：探测器在失败后，被视为成功的最小连续成功数。默认值是 1。 存活和启动探测的这个值必须是 1。最小值是 1。

`failureThreshold`：当探测失败时，Kubernetes 的重试次数。 存活探测情况下的放弃就意味着重新启动容器。 就绪探测情况下的放弃 Pod 会被打上未就绪的标签。默认值是 3。最小值是 1。

kdemo-0810-probe.yaml

```shell
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kdemo-0810-probe
spec:
  selector:
    matchLabels:
      app: kdemo-0810-probe
  replicas: 2
  template:
    metadata:
      labels:
        app: kdemo-0810-probe
    spec:
      containers:
      - name: kdemo-0810-probe
        image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
        ports:
        - containerPort: 80
        # 存活检查
        livenessProbe:
          failureThreshold: 1
          httpGet:
            path: /index.php
            port: 80
            scheme: HTTP
          initialDelaySeconds: 5
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 10
        # 就绪检查
        readinessProbe:
          failureThreshold: 1
          httpGet:
            path: /index.php
            port: 80
            scheme: HTTP
          initialDelaySeconds: 5
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 10
```

```shell
$ kubectl apply -f kdemo-0810-probe.yaml
deployment.apps/kdemo-0810 created
```

### 六、设置容器的环境变量

通过yaml来创建deployment

kdemo-0810-env.yaml

```shell
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kdemo-0810-env
spec:
  selector:
    matchLabels:
      app: kdemo-0810-env
  replicas: 2
  template:
    metadata:
      labels:
        app: kdemo-0810-env
    spec:
      containers:
      - name: kdemo-0810-env
        image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
        ports:
        - containerPort: 80
        env:
        - name: MYSQL_HOST
          value: "mysql-57"
        - name: MYSQL_DB_NAME
          value: "basic"
        - name: MYSQL_DB_USER
          value: "root"
        - name: MYSQL_DB_PASSWORD
          value: "password"
```

创建Deployment

```shell
$ kubectl apply -f kdemo-0810-env.yaml
deployment.apps/kdemo-0810 created
```

查看deployment是否创建成功

```shell
$ kubectl get pods
NAME                                READY   STATUS              RESTARTS   AGE
kdemo-5d64d6f667-7fmt8   0/1     ContainerCreating   0          6s
kdemo-5d64d6f667-wchwv   0/1     ContainerCreating   0          6s
```

验证环境变量是否生效

```shell
$ kubectl exec -it kdemo-5d64d6f667-7fmt8 -- bash
$ echo $MYSQL_DB_PASSWORD
basic
```

### 七、为容器分配资源

kdemo-0801-resources-limit.yaml

```shell
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kdemo-0810-resources-limit
spec:
  selector:
    matchLabels:
      app: kdemo-0810-resources-limit
  replicas: 2
  template:
    metadata:
      labels:
        app: kdemo-0810-resources-limit
    spec:
      containers:
      - name: kdemo-0810-resources-limit
        image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
        env:
        - name: MYSQL_HOST
          value: "mysql-57"
        - name: MYSQL_DB_NAME
          value: "basic"
        - name: MYSQL_DB_USER
          value: "root"
        - name: MYSQL_DB_PASSWORD
          value: "password"
        ports:
        - containerPort: 80
        # 添加资源限制
        resources:
          limits:
            cpu: "1"
            memory: "200Mi"
          requests:
            cpu: "0.5"
            memory: "100Mi"
```

```shell
# 安装stress进行资源限制测试
$ apt install stress

# 产生 2 个进程做随机数开方计算
$ stress -c 2

# 产生 2 个进程，每个进程分配 100M 内存：
$ stress --vm 2 --vm-bytes 100M --vm-keep
```

### 八、利用亲和性把pod分布到指定的node

给vm-k8s-node-2添加一个标签 `disk = large`

```shell
$ kubectl label nodes vm-k8s-node-2 disk=large

$ kubectl get nodes --show-labels
NAME            STATUS   ROLES                  AGE    VERSION   LABELS
vm-k8s-master   Ready    control-plane,master   4d1h   v1.21.3   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=vm-k8s-master,kubernetes.io/os=linux,node-role.kubernetes.io/control-plane=,node-role.kubernetes.io/master=,node.kubernetes.io/exclude-from-external-load-balancers=
vm-k8s-node-1   Ready    <none>                 26h    v1.21.3   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=vm-k8s-node-1,kubernetes.io/os=linux
vm-k8s-node-2   Ready    <none>                 26h    v1.21.3   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,disk=large,kubernetes.io/arch=amd64,kubernetes.io/hostname=vm-k8s-node-2,kubernetes.io/os=linux
```

```shell
$ kubectl edit pod <pod-name>

apiVersion: apps/v1
kind: Deployment
metadata:
  name: kdemo-0810-node-selector
spec:
  selector:
    matchLabels:
      app: kdemo-0810-node-selector
  replicas: 2
  template:
    metadata:
      labels:
        app: kdemo-0810-node-selector
    spec:
      containers:
      - name: kdemo-0810-node-selector
        image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
        env:
        - name: MYSQL_HOST
          value: "mysql-57"
        - name: MYSQL_DB_NAME
          value: "basic"
        - name: MYSQL_DB_USER
          value: "root"
        - name: MYSQL_DB_PASSWORD
          value: "password"
        ports:
        - containerPort: 80
        # 使用节点选择器
      nodeSelector:
        disk: large
```

##### 节点亲和性

`requiredDuringSchedulingIgnoredDuringExecution`：必须部署到满足条件的节点上，如果没有满足条件的节点，就不断重试

`preferredDuringSchedulingIgnoredDuringExecution`：优先部署在满足条件的节点上，如果没有满足条件的节点，就忽略这些条件，按照正常逻辑部署

### 九、作业

1、将 MySQL 迁移到 Kubernetes 集群，创建服务，并配置环境变量，使用 kdemo 连接数据库，使用服务名连接

2、让 kdemo 和 MySQL 部署到不同的节点

