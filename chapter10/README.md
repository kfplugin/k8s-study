```shell
#: 注释
$: 可执行命令
其他: 终端输出
```

# 一、Helm介绍与安装

**Helm是一个Kubernetes包管理器**

Helm 帮助您管理 Kubernetes 应用——Helm chart，即使是最复杂的 Kubernetes 应用程序，都可以帮助您定义，安装和升级。

Chart 能够让你更加容易地创建、发版、分享和发布，开始使用 Helm 并且停止复制粘贴吧。

Helm 是 CNCF 的毕业项目，由 Helm 社区维护。

### 思考实际场景

![apply](./assets/README-1631117480049.png)

实际业务的应用，是有多个微服务组成，其中还包含各类组件，所以一个应用需要有多个yaml文件。所以在部署的时候，需要全部通过kubectl apply方式一个个起起来，非常费劲。

另外一个问题是，在不同环境时，可能配置不一样，yaml文件的内容也是不一样的，所以就需要有多份不同的yaml文件来适配不同的环境。

helm就是为了解决以上场景，提供一种包管理能力。

### Helm的作用

![helm](./assets/README-1631150866821.png)

Helm使用Chart来维护yaml模板文件和模板中的值，可以把多个yaml模板打成一个包，并且提供一件安装部署的能力。

由于解耦了templates和values，一套yaml模板可以应用指定的values文件，实现在不同环境下灵活发布。

Helm还提供升级、回滚等功能，可以轻松地做版本切换。

### 安装Helm

下载helm工具

```shell
$ wget https://get.helm.sh/helm-v3.4.1-linux-amd64.tar.gz -O helm-v3.4.1-linux-amd64.tar.gz
```

解压并拷贝，放到/usr/local/bin目录下

```shell
$ tar -zxvf  helm-v3.4.1-linux-amd64.tar.gz
$ mv linux-amd64/helm /usr/local/bin/helm
```

查看版本

```shell
$ helm verison
-----------------------------
version.BuildInfo{Version:"v3.4.1", GitCommit:"c4e74854886b2efe3321e185578e6db9be0a6e29", GitTreeState:"clean", GoVersion:"go1.14.11"}
```

# 二、Helm的应用

Helm利用Chart来管理包，通过Charts.yaml文件中定义，包括版本，应用名称等

利用templates来定义服务，并且从模板文件中抽取变量出来，放到values.yaml文件中，如下图：

![template-value](./assets/README-1631254445309.png)

helm支持以下操作：

1. **helm create xxx**：创建一个chart
2. **helm package xxx**：打包
3. **helm install xxx xxx-0.1.0.tgz**：从本地文件包中安装部署应用
4. **helm install xxx <仓库名>/<包名>**：从仓库中安装部署应用
5. **helm delete xxx**：卸载应用
6. **helm repo add xxx**：添加helm仓库
7. **helm repo update**：从仓库中更新
8. **helm upgrade xxx <仓库名>/<包名> --version 0.1.1**: 从仓库中升级应用
9. **helm rollback x**: 回滚应用版本

# 三、JCR私有化仓库

## 2. JCR仓库搭建

在 Master 节点上执行 vi ~/.bash_profile，增加 JFROG_HOME 环境变量

```shell
export $JFROG_HOME=/etc
```

source ~/.bash_profile

创建目录

```shell
$ mkdir -p $JFROG_HOME/artifactory/var/
$ chmod -R 777 $JFROG_HOME/artifactory/var
```

启动JCR

```shell
docker run --name artifactory-jcr -v $JFROG_HOME/artifactory/var/:/var/opt/jfrog/artifactory -d -p 8081:8081 -p 8082:8082 registry.cn-shenzhen.aliyuncs.com/jayzone/artifactory-jcr:7.4.3
```

配置hosts

```shell
192.168.1.101 master artifactory.local
192.168.1.102 node1
192.168.1.103 node2
```

修改/etc/docker/daemon.json， 增加"insecure-registries": ["artifactory.local:8081"]

```shell
{
  ...
    "insecure-registries": ["artifactory.local:8081"]
  ...
}
```

重启docker

```shell
$ systemctl restart docker
```

登录JCR仓库，并初始化。访问地址：http://192.168.1.101:8081，登录账号密码: admin password

一路跳过后来到选择初始化仓库页面，勾选docker和helm

![jcr](./assets/README-1631260059713.png)

初始化后，创建两个仓库 helm-test-local 和 helm-prod-local

![local](./assets/README-1631260154274.png)

获取仓库地址

![setmeup](./assets/README-1631260413129.png)

![f](./assets/README-1631260443266.png)

# 四、实践

## 1. 利用Helm打包、部署kdemo

创建一个chart

```shell
$ helm create kdemo
```

查看目录

```shell
$ ls -l kdemo
------------------------------------
总用量 8
drwxr-xr-x 2 root root    6 9月  10 01:43 charts
-rw-r--r-- 1 root root 1096 9月  10 01:43 Chart.yaml
drwxr-xr-x 3 root root  169 9月  10 01:44 templates
-rw-r--r-- 1 root root 1798 9月  10 01:43 values.yaml
```

把kdemo所需要的yaml拷贝到templates目录中（templates会默认有一些示例代码，直接删掉即可）

```shell
$ rm -rf kdemo/templates/*.*
$ cp -R ~/k8s-study/chapter10/yaml/* kdemo/templates
```

```shell
$ ls -l
-----------------------------------------------
总用量 24
-rw-r--r-- 1 root root 1762 9月  10 01:43 _helpers.tpl
-rw-r--r-- 1 root root  396 9月  10 01:44 kdemo-configmap.yaml
-rw-r--r-- 1 root root 1249 9月  10 01:44 kdemo-deployment.yaml
-rw-r--r-- 1 root root  246 9月  10 01:44 kdemo-service.yaml
-rw-r--r-- 1 root root 1020 9月  10 01:44 mysql-statefulset.yaml
-rw-r--r-- 1 root root 1735 9月  10 01:43 NOTES.txt
drwxr-xr-x 2 root root   34 9月  10 01:43 tests
```

修改values.yaml文件

```shell
replicaCount: 1

mysql:
  host: mysql57.default.svc.cluster.local
  port: 3306
  dbname: basic
  pass: password
  
service:
  nodePort: 30080
```

主要是与mysql相关的额配置信息

把模板文件中需要用到values.yaml文件中的变量的地方，修改成{{ .Values.xxx.xxx }}方式取值

修改 kdemo-configmap.yaml 为如下：

```shell
apiVersion: v1
kind: ConfigMap
metadata:
  name: kdemo-configmap
data:
  MYSQL_HOST: "{{ .Values.mysql.host }}"
  MYSQL_PORT: "{{ .Values.mysql.port }}"
  MYSQL_USER: "root"
  MYSQL_DB_NAME: "{{ .Values.mysql.dbname }}"
  MYSQL_DB_PASSWORD: "{{ .Values.mysql.pass }}"
  params: |
    <?php

    return [
        'adminEmail' => 'blake@kdemo.com',
        'senderEmail' => 'noreply@kdemo.com',
        'senderName' => 'kdemo.com mailer',
    ];
```

修改 kdemo-deployment.yaml

```shell
...
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: kdemo
....    
```

kdemo的chart文件我已经放到了k8s-study/chapter10/chart/kdemo中，可以自取并比较

```shell
$ git clone https://oauth2:tmsHZgob-AQNA9C7-BD5@git.mysre.cn/jintang/k8s-study.git
$ cd k8s-study/chapter10/chart
```

打包kdemo应用

```shell
$ helm package kdemo
------------------------------
Successfully packaged chart and saved it to: /root/helm/kdemo-0.1.0.tgz
```

部署kdemo应用

```shell
$ helm install kdemo kdemo-0.1.0.tgz
--------------------------------------
NAME: kdemo
LAST DEPLOYED: Fri Sep 10 03:22:41 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

说明已经可以了，查看pod,svc,cm

```shell
$ kubectl get po,svc
---------------------------------------
NAME                         READY   STATUS    RESTARTS   AGE
pod/kdemo-7c7c45d784-jcqr7   1/1     Running   0          97s
pod/mysql-0                  1/1     Running   0          97s

NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
service/kdemo        NodePort    10.1.206.250   <none>        80:30080/TCP     97s
service/kubernetes   ClusterIP   10.1.0.1       <none>        443/TCP          35d
service/mysql57      NodePort    10.1.224.60    <none>        3306:30306/TCP   97s

NAME                        DATA   AGE
configmap/kdemo-configmap   6      97s
```

浏览器访问：[http://192.168.1.102:30080](http://192.168.1.102:30080)

## 2. 上传kdemo到JCR仓库

注意配置和上传的地址都可以通过JCR后台 Application > JFrog Container Registry > Artifacts 中的 "Set Me Up"按钮后的页面中找到。

配置 Helm 仓库

```shell
$ helm repo add helm http://192.168.1.101:8081/artifactory/helm --username admin --password APAmxeEqnsiLgKikiw3bEvWfFtB
------------------------------------------------------
"helm" has been added to your repositories
```

上传

```shell
$ curl -uadmin:APAmxeEqnsiLgKikiw3bEvWfFtB -T kdemo-0.1.1.tgz  "http://192.168.1.101:8082/artifactory/helm-local/kdemo-0.1.0.tgz"
```

从 JCR 部署 Chart

```shell
$ helm repo update
$ helm install kdemo helm/kdemo
```

修改副本数为3

```shell
$ vim kdemo/values.yaml
-------------------------------
replicaCount: 3
```

修改Charts.yaml的版本好号0.1.1

```shell
$ vim kdemo/Charts.yaml
-------------------------------
version: 0.1.1
```

然后重新打包

```shell
$ helm package kdemo
---------------------------
Successfully packaged chart and saved it to: /root/helm/kdemo-0.1.1.tgz
```

上传 chart 0.1.1版本

```shell
$ curl -uadmin:AP5pSiYer3aogCvz9F7u3P5qFxx -T kdemo-0.1.0.tgz "http://192.168.1.101:8081/artifactory/helm-local/kdemo-0.1.1.tgz"
```

获取新版本 Chart

```shell
$ helm repo update
```

部署新版本 Chart

```shell
$ helm upgrade kdemo helm/kdemo --version 0.1.1
----------------------------------------------------
Release "kdemo" has been upgraded. Happy Helming!
NAME: kdemo
LAST DEPLOYED: Sat Sep 11 00:28:52 2021
NAMESPACE: default
STATUS: deployed
REVISION: 2
TEST SUITE: None
```

查看pod数

```shell
$ kubectl get po
-------------------------------------------------
NAME                     READY   STATUS    RESTARTS   AGE
kdemo-7c7c45d784-98crr   1/1     Running   0          11m
kdemo-7c7c45d784-k49tn   1/1     Running   0          45s
kdemo-7c7c45d784-whpk7   1/1     Running   0          45s
mysql-0                  1/1     Running   0          11m
```

回滚版本到0.1.0

```shell
$ helm rollback kdemo 1
------------------------------------
[root@master helm]# helm rollback kdemo 1
Rollback was a success! Happy Helming!
````

再次查看pod数

```shell
[root@master helm]# kubectl get po
NAME                     READY   STATUS        RESTARTS   AGE
kdemo-7c7c45d784-98crr   1/1     Running       0          14m
kdemo-7c7c45d784-k49tn   1/1     Terminating   0          3m44s
kdemo-7c7c45d784-whpk7   1/1     Terminating   0          3m44s
mysql-0                  1/1     Running       0          14m
```
## 3. 在不同环境下部署kdemo

编写values-test.yaml文件

```shell
$ cp values.yaml values-test.yaml
$ cp values.yaml values-prod.yaml
$ vim values-test.yaml
-------------------------------------
replicaCount: 1

mysql:
  host: mysql57.default.svc.cluster.local
  port: 3306
  dbname: basicTest # 修改数据库名为basicTest
  pass: password
```

在测试空间测试新版本

```shell
$ helm install kdemo kdemo-0.1.0.tgz -f kdemo/values-test.yaml -n test
```

在生产空间生产新版本

```shell
$ helm install kdemo kdemo-0.1.0.tgz -f kdemo/values-prod.yaml -n prod
```

# 五、作业

1. 熟悉JCR仓库，并且把上传kdemo镜像到JCR仓库中
2. 在JCR仓库中，分别创建helm-test-local，helm-prod-local两个仓库，并且把不同版本的values打包上传到对应的仓库中，实现一键部署