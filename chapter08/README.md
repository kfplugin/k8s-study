# 私有化镜像中心搭建实践

## 什么是镜像
- 理解

    Docker镜像是Docker容器允许时的只读模版，每一个镜像由一系列的层 (layers) 组成。

- 镜像的特性

    容器有不可改变性（immutability）和可移植性（portability）。容器把应用的可执行文件、依赖文件及操作系统文件等打包成镜像，使应用的运行环境固定下来不再变化；同时，镜像可在其他环境下重现同样的运行环境。这些特性给运维和应用的发布带来极大的便利，这要归功于封装应用的镜像。

- 镜像的结构

  容器有不可改变性（immutability）和可移植性（portability）。容器把应用的可执行文件、依赖文件及操作系统文件等打包成镜像，使应用的运行环境固定下来不再变化；同时，镜像可在其他环境下重现同样的运行环境。这些特性给运维和应用的发布带来极大的便利，这要归功于封装应用的镜像。

- 镜像的生成
    - docker commit
      
      这种方法适用于试验性的镜像，用户在容器中执行各种操作，达到某种合乎要求的状态时，用“dockercommit”命令把容器的状态固化下来成为镜像。由于该方法需要用户手动输入命令，因此不适合在自动化流水线里面使用。
    - docker build
      
      通常镜像是由“docker build”命令依照Dockerfile构建的，Dockerfile描述了镜像包含的所有内容和配置信息（如启动命令等）
    
        - Dockerfile实践
            ```dockerfile
            FROM php:7.3-fpm-alpine
            # 安装nginx
            RUN apk update --no-cache
            RUN apk add nginx --nocache
            # 暴露端口
            EXPOSE 80
            CMD ["nginx", "-g", "daemon off;"] 
            ```
        - `docker build -t phpnginx:1.0 .`
            ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629885683603.png?version=1&modificationDate=1629886780000&api=v2)
    
    - docker分层查看：`docker history <image-id>`
        ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629885745167.png?version=1&modificationDate=1629886780000&api=v2)

## 公有镜像仓库

### 官方公有镜像仓库
- Docker官方镜像：https://registry.hub.docker.com

### 阿里云镜像仓库
- 地址：https://cr.console.aliyun.com

## Harbor私有镜像仓库搭建

### 1. 为什么需要私有镜像仓库

- 安全
  
    我们的代码和配置很多都打包到镜像当中，只要一启动容器进去都基本能看到，所以需要使用私有化镜像来保证别人无法pull我们都镜像。

- 快速

    私有化镜像仓库一般都部署到内网当中，在pull的时候直接从内网pull，这样肯定比公网pull快很多。

### 2. harbor架构
- 架构图
    ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629885249120.png?version=1&modificationDate=1629886780000&api=v2)

- registry

    主要是用于存放docker镜像的服务，可以在docker官方镜像仓库中拉取并启动。官方文档可以查看：https://docs.docker.com/registry/


### 3. 单机部署harbor
#### 1. 虚拟机准备。
- 我这边准备的是一个docker-base的虚拟机，直接克隆一份就可以了。
  
    ![虚拟机](https://confluence.mysre.cn/download/attachments/46513214/README-1629287046341.png?version=1&modificationDate=1629886780000&api=v2)

- 修改网络信息，修改mac地址，可以重新生成新的IP地址
  
    ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629287178325.png?version=1&modificationDate=1629886780000&api=v2)
    ![2](https://confluence.mysre.cn/download/attachments/46513214/README-1629287198153.png?version=1&modificationDate=1629886780000&api=v2)

- 启动虚拟机，查看IP地址
  
    ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629287291467.png?version=1&modificationDate=1629886780000&api=v2)

- 设置静态IP地址
  
    ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629287437890.png?version=1&modificationDate=1629886780000&api=v2)
    ![2](https://confluence.mysre.cn/download/attachments/46513214/README-1629287493255.png?version=1&modificationDate=1629886780000&api=v2)

- 安装docker-compose 
    > Compose 是用于定义和运行多容器 Docker 应用程序的工具。通过 Compose，您可以使用 YML 文件来配置应用程序需要的所有服务。然后，使用一个命令，就可以从 YML 文件配置中创建并启动所有服务。
    - 直接下载docker-compose可执行文件：https://github.com/docker/compose/releases
    - 移动到/usr目录下`mv docker-compose-Linux-x86_64 /usr/local/bin/docker-compose`
    - 配置权限：`chmod +x /usr/local/bin/docker-compose`
    - 查看版本：`docker-compose version`
      
        ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629337297272.png?version=1&modificationDate=1629886780000&api=v2)

#### 2. 安装harbor
- harbor官网:https://goharbor.io/
- 下载harbor安装文件
    - 下载地址：https://github.com/goharbor/harbor/releases
    - 两种选择
        - 在线安装(推荐，毕竟github下东西贼慢)
        - 离线安装
    - 选择在线安装包：harbor-online-installer-v2.3.1.tgz
      
      ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629290394400.png?version=1&modificationDate=1629886780000&api=v2)
    - 解压文件 `tar -zxvf harbor-online-installer-v2.3.1.tgz && cd harbor`
    - 复制配置文件 `cp harbor.yml.tmpl harbor.yml`
    - 查看配置文件
        ```yaml
            # Harbor配置文件
            # 访问管理UI和注册表服务的IP地址或主机名
            # 不要使用localhost或127.0.0.1，因为Harbor需要由外部客户端访问。
            hostname: reg.mydomain.com
            
            # http相关配置
            http:
            # http的端口，默认是80。如果启用https，该端口将重定向到https端口
            port: 80
            
            # https相关配置
            https:
            # https端口，默认为443
            port: 443
            # nginx的cert和key文件路径
            certificate: /your/certificate/path
            private_key: /your/private/key/path
            
            # # 取消注释以下将启用tls通信之间的所有harbor组件
            # internal_tls:
            #   # “启用”为“true”表示启用内部TLS
            #   enabled: true
            #   # 将您的证书和密钥文件放在dir上
            #   dir: /etc/harbor/tls/internal
            
            # 如果要启用外部代理，请取消注释external_url
            # 当它启用时，主机名将不再使用
            # external_url: https://reg.mydomain.com:8433
            
            # harbour admin的初始密码
            # 只有在第一次安装Harbor时才会起作用
            # 记住，启动Harbor后，从UI更改管理员密码。
            harbor_admin_password: Harbor12345
            
            # Harbor 数据库配置
            database:
            # DB端口root用户的密码。在任何生产使用之前更改此参数。
            password: root123
            # 空闲连接池中的最大连接数。如果它<=0，则没有空闲连接被保留。
            max_idle_conns: 100
            # 到数据库的最大打开连接数。如果它<= 0，则对打开的连接数没有限制。
            # 注意:端口postgres的默认连接数是1024。
            max_open_conns: 900
            
            # 默认数据卷
            data_volume: /data
            
            # 默认情况下，Harbor存储设置是使用/data dir在本地文件系统上
            # 取消“storage_service setting”注释如果需要使用外部存储
            # storage_service:
            #   # Ca_bundle是自定义根ca证书的路径，该证书将被注入信任存储区
            #   # 注册表和图表存储库的容器。当用户托管带有自签名证书的内部存储时，通常需要这样做。
            #   ca_bundle:
            
            #   # 存储后端，默认是文件系统，选项包括文件系统，azure, gcs, s3, swift和oss
            #   # 有关此配置的更多信息，请参考https://docs.docker.com/registry/configuration/
            #   filesystem:
            #     maxthreads: 100
            #   # 当您想要禁用注册表重定向时，将禁用设置为true
            #   redirect:
            #     disabled: false
            
            # Trivy配置
            #
            # Trivy DB包含来自NVD、Red Hat和许多其他上游漏洞数据库的漏洞信息。
            # Trivy从GitHub发布页面https://github.com/aquasecurity/trivy-db/releases下载并缓存
            # 在本地文件系统中。此外，数据库包含更新时间戳，因此Trivy可以检测它是否
            # 应该从互联网上下载一个更新的版本或使用缓存的版本。目前，数据库每次都要更新
            # 在GitHub上发布了一个新版本。
            trivy:
            # ignoreUnfixed只显示固定漏洞的标志
            ignore_unfixed: false
            # skipUpdate启用或禁用从GitHub下载Trivy DB的标志
            #
            # 您可能希望在测试或CI/CD环境中启用此标志，以避免GitHub速率限制问题。
            # 如果启用了这个标志，你必须手动下载' trivy-offline.tar.gz '存档，提取' trivy.db '和' metadata '。Json '文件，并挂载到' /home/scanner/。缓存/ trivy / db的路径。
            skip_update: false
            #
            # insecure跳过验证注册表证书的标志
            insecure: false
            # github_token下载Trivy DB的GitHub访问令牌
            #
            # GitHub上的匿名下载限制在每小时60次。通常这样的速率限制对于生产操作来说是足够的。如果，出于任何原因，这还不够，你可以增加速率限制到5000
            # 通过指定GitHub访问令牌，每小时请求数。关于GitHub利率限制的更多细节，请咨询://developer.github.com/v3/#rate-limiting
            #
            # 你可以按照下面的说明来创建GitHub令牌
            # https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line
            #
            # github_token: xxx
            
            jobservice:
            # 在作业服务中最大的作业工人数量
            max_job_workers: 10
            
            notification:
            # webhook作业的最大重试计数
            webhook_job_max_retry: 10
            
            chart:
            # 将absolute_url的值更改为enabled可以在图表中启用绝对url
            absolute_url: disabled
            
            # 日志配置信息
            log:
            # 日志选项有： debug, info, warning, error, fatal
            level: info
            # 配置本地存储的日志
            local:
            # 日志文件在被删除之前会被旋转log_rotate_count次。如果count为0，则删除旧版本，而不是旋转旧版本。
            rotate_count: 50
            # 只有当日志文件的长度大于log_rotate_size字节时，日志文件才会被旋转。如果size后面跟着k，则假设size的单位是千字节。
            # 如果使用M，大小为兆字节，如果使用G，大小为千兆字节。尺寸是100,100k, 100M, 100G都是有效的。
            rotate_size: 200M
            # 存储日志的主机上的目录
            location: /var/log/harbor
            
            # 取消注释以下行以启用外部syslog端点。
            # external_endpoint:
            #   # 用于向外部终端发送日志的协议，选项为TCP或udp
            #   protocol: tcp
            #   # 外部端点的主机
            #   host: localhost
            #   # 外部端点端口
            #   port: 5140
            
            # 这个属性是用来检测。cfg文件的版本的，不要修改!
            _version: 2.3.0
            
            # 如果使用外部数据库，取消注释external_database。
            # external_database:
            #   harbor:
            #     host: harbor_db_host
            #     port: harbor_db_port
            #     db_name: harbor_db_name
            #     username: harbor_db_username
            #     password: harbor_db_password
            #     ssl_mode: disable
            #     max_idle_conns: 2
            #     max_open_conns: 0
            #   notary_signer:
            #     host: notary_signer_db_host
            #     port: notary_signer_db_port
            #     db_name: notary_signer_db_name
            #     username: notary_signer_db_username
            #     password: notary_signer_db_password
            #     ssl_mode: disable
            #   notary_server:
            #     host: notary_server_db_host
            #     port: notary_server_db_port
            #     db_name: notary_server_db_name
            #     username: notary_server_db_username
            #     password: notary_server_db_password
            #     ssl_mode: disable
            
            # 如果使用外部Redis服务器，取消注释external_redis
            # external_redis:
            #   # support redis, redis+sentinel
            #   # host for redis: <host_redis>:<port_redis>
            #   # host for redis+sentinel:
            #   #  <host_sentinel1>:<port_sentinel1>,<host_sentinel2>:<port_sentinel2>,<host_sentinel3>:<port_sentinel3>
            #   host: redis:6379
            #   password:
            #   # sentinel_master_set must be set to support redis+sentinel
            #   #sentinel_master_set:
            #   # db_index 0 is for core, it's unchangeable
            #   registry_db_index: 1
            #   jobservice_db_index: 2
            #   chartmuseum_db_index: 3
            #   trivy_db_index: 5
            #   idle_timeout_seconds: 30
            
            # 取消对uaa的注释，因为它信任通过自签名证书托管的uaa实例的证书。
            # uaa:
            #   ca_file: /path/to/ca
            
            # 全球代理
            # 配置组件的http代理，例如http://my.proxy.com:3128
            # 组件不需要通过http代理相互连接。
            # 如果想要禁用代理，请从' components '数组中删除组件
            # 如果要使用代理进行复制，必须启用代理
            # 用于core和jobservice，并设置“http_proxy”和“https_proxy”。
            # 添加域到' no_proxy '字段，当你想禁用代理
            #用于某些特殊的注册表。
            proxy:
            http_proxy:
            https_proxy:
            no_proxy:
            components:
            - core
            - jobservice
            - trivy
            
            # metric:
            #   enabled: false
            #   port: 9090
            #   path: /metrics
        ```
 - 修改hostname和端口(端口可不修改)
    ```yaml
        ....
        hostname: 192.168.10.150
        ....
        http:
        port: 8080
        ....
        # 本地访问，不需要https
        # https:
        # https端口，默认为443
        # port: 443
        ....
    ```
    - 执行安装命令 `sh install.sh`， 注意：时间会比较久，因为要拉取镜像。
    - 安装成功后，访问web页面：http://192.168.10.150:8080
      
        ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629365520894.png?version=1&modificationDate=1629886780000&api=v2)

### helm部署

由于helm还没讲到，这里就不做发散，有兴趣的可以参考文档自己玩玩：https://artifacthub.io/packages/helm/harbor/harbor

## Harbor私有镜像仓库使用
- 登录harbor Web管理后台
    - 账号：admin
    - 密码：Harbor12345
- 创建一个项目：k8s-study
  
    ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629366897708.png?version=1&modificationDate=1629886780000&api=v2)
- 推送一个镜像到harbor
    - 调整docker配置，让docker支持http：`vim /etc/docker/daemon.json`
        ```json
            {
              "insecure-registries":["192.168.10.150:8080"] 
            }
        ```
    - 重启docker：`systemctl restart docker`
    - docker登录：`docker login http://192.168.10.150:8080`
      
        ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629367675864.png?version=1&modificationDate=1629886780000&api=v2)
    - 先pull一个nginx镜像：`docker pull nginx:1.21.1-alpine`
    - 将nginx重新标记，将其归到我们到harbor镜像仓库：`docker tag nginx:1.21.1-alpine 192.168.10.150:8080/k8s-study/nginx:1.21.1-alpine`
      
        ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629371552403.png?version=1&modificationDate=1629886780000&api=v2)
    - 推送镜像到harbor：`docker push 192.168.10.150:8080/k8s-study/nginx:1.21.1-alpine`
      
        ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629371622187.png?version=1&modificationDate=1629886780000&api=v2)
        ![2](https://confluence.mysre.cn/download/attachments/46513214/README-1629371652324.png?version=1&modificationDate=1629886780000&api=v2)
        ![3](https://confluence.mysre.cn/download/attachments/46513214/README-1629371700740.png?version=1&modificationDate=1629886780000&api=v2)

- 拉取harbor中到镜像
    - 先删除上一步产生到nginx镜像：`docker rmi 192.168.10.150:8080/k8s-study/nginx:1.21.1-alpine`
    - 拉取镜像：`docker pull 192.168.10.150:8080/k8s-study/nginx:1.21.1-alpine`
      
        ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629371872178.png?version=1&modificationDate=1629886780000&api=v2)

## Kubernetes中使用私有化镜像

- 创建docker私有化镜像仓库
    ```shell
      kubectl create secret docker-registry harbor --docker-server=192.168.10.150:8080 --docker-username=admin --docker-password=Harbor12345
    ```
- 查看secret：`kubectl get secrets`
  
    ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629373268722.png?version=1&modificationDate=1629886780000&api=v2)

- 创建nginx.yaml文件
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: nginx-web
      labels:
        app: nginx-web
    spec:
      replicas: 1
      template:
        metadata:
          name: nginx-web
          labels:
            app: nginx-web
        spec:
          imagePullSecrets:
            - name: harbor
          containers:
            - name: nginx-web
              image: 192.168.10.150:8080/k8s-study/nginx:1.21.1-alpine
              imagePullPolicy: IfNotPresent
              ports:
                - containerPort: 80
          restartPolicy: Always
      selector:
        matchLabels:
          app: nginx-web
    
    ---
    
    apiVersion: v1
    kind: Service
    metadata:
      name: nginx-web
    spec:
      selector:
        app: nginx-web
      ports:
        - port: 80
          targetPort: 80
      type: NodePort
    ```

- 部署nginx:`kubectl apply -f nginx.yaml`
  
    ![1](https://confluence.mysre.cn/download/attachments/46513214/README-1629376931850.png?version=1&modificationDate=1629886780000&api=v2)
    ![2](https://confluence.mysre.cn/download/attachments/46513214/README-1629376971909.png?version=1&modificationDate=1629886780000&api=v2)
  
## 作业
1. 搭建harbor镜像仓库。
2. 将kdemo打包为镜像上传到harbor镜像仓库中。
3. 将harbor中到kdemo镜像发布到kubernetes中。