# 后端专业线kubernetes实践课

## 基础篇

- chapter01 环境安装、docker及应用准备
- chapter02 Kubernetes集群搭建实践
- chapter03 Pod基础调度实践
- chapter04 Service网络实践
- chapter05 应用程序生命周期管理实践
- chapter06 持久化存储数据实践
- chapter07 基于RBAC的权限管理实践
- chapter08 私有镜像中心搭建实践
- chapter09 Ingress与网络策略
- chapter10 helm部署应用实践