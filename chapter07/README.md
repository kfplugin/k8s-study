# 一、Kubernetes的安全框架

![安全框架](./assets/README-1629772977854.png)

K8S安全控制框架主要由下面3个阶段进行控制，每一个阶段都 支持插件方式，通过API Server配置来启用插件。

1. Authentication（鉴权）
2. Authorization（授权）
3. Admission Control（准入控制）

客户端要想访问K8s集群API Server，一般需要证书、Token或者用户名+密码；如果Pod访问，需要ServiceAccount

## 鉴权 Authentication

三种客户端的认证方式：

1. HTTPS 证书认证：基于CA证书签名的数字证书认证
2. HTTP Token认证：通过一个Token来识别用户
3. HTTP Base认证：用户名+密码的方式

## 授权 Authorization

BAC（Role-Based Access Control，基于角色的访问控制）：负责完成授权（Authorization）工作。

RBAC根据API请求属性，决定允许还是拒绝。

比较常见的授权维度：

- user：用户名
- group：用户分组
- 资源，例如pod、deployment
- 资源操作方法：get，list，create，update，patch，watch，delete
- 命名空间
- API组

## 准入控制

Adminssion Control实际上是一个准入控制器插件列表，发送到API Server 的请求都需要经过这个列表中的每个准入控制器插件的检查，检查不通过，则拒绝请求。

[https://kubernetes.io/zh/docs/reference/access-authn-authz/admission-controllers/](https://kubernetes.io/zh/docs/reference/access-authn-authz/admission-controllers/)

由于 kubeadm 集群工具是通过pod来启动 apisever 的，所以可以在master上查看一下 apiserver 的pod启动时候的命令：

```shell
$ kubectl get po -n kube-system
-------------------------------
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-7f4f5bf95d-rh88s   1/1     Running   3          8d
calico-node-48tb2                          1/1     Running   0          8d
calico-node-5fbx5                          1/1     Running   1          8d
calico-node-cvhq6                          1/1     Running   1          8d
coredns-6d56c8448f-6xkxn                   1/1     Running   1          8d
coredns-6d56c8448f-jtls2                   1/1     Running   1          8d
etcd-master                                1/1     Running   2          8d
kube-apiserver-master                      1/1     Running   4          8d
kube-controller-manager-master             1/1     Running   3          26h
kube-proxy-2kvkn                           1/1     Running   0          30h
kube-proxy-llbtn                           1/1     Running   0          30h
kube-proxy-lns9p                           1/1     Running   0          30h
kube-scheduler-master                      1/1     Running   3          26h
```

查看 kube-apiserver-master

```shell
$ kubectl describe pod kube-apiserver-master -n kube-system
```

![apiserver-enable](./assets/README-1629777891172.png)

发现 --enable-admission-plugins 参数的值为 NodeRestriction

**NodeRestriction** 该准入控制器限制了 kubelet 可以修改的 Node 和 Pod 对象。

# 二、基于角色的权限访问控制（RBAC）

## kubernetes的RBAC

![rbac](./assets/README-1629771960549.png)

RBAC（Role-Based Access Control，基于角色的访问控制），允许通过Kubernetes API动态配置策略。

### 主体（Subject）

- User 用户
- Group 用户组
- ServiceAccount 服务账号

### 角色（Role）

- Role 授权特定命名空间的访问权限
- ClusterRole 授权所有命名空间的访问权限

### 角色绑定（RoleBinding）

- RoleBinding：将角色绑定到主体（即subject）
- ClusterRoleBinding：将集群角色绑定到主体

## RBAC配置示例

角色定义

```shell
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: default
  name: pod-reader
rules:
  - apiGroups: ["", "apps"]
    resources: ["pods", "services", "deployments"]
    verbs: ["get", "watch", "list"]
````

apiGroups可以通过一下命令查看：

![apiResources](./assets/README-1629773640699.png)

注意：

1. 上面示例配置中的apiGroups指的是上图中的kubectl api-resources命令返回结果中的APIGROUP列，如果为空，则表示系统定义。如果要控制deployments，则需要填上apps
2. Role和RoleBinding是特定命名空间下的角色，所以需要指定namespace

角色绑定

```shell
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: read-pods
  namespace: default
subjects:
  - kind: User # 此处使用的主体是User类型，这里的kind字段与首行的kind字段不是一个概念
    name: blake
    apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```

说明：由于Role与RoleBinding都是属于rbac.authorization.k8s.io/v1这个apiGroup下，所以都是需要指定的。

# 三、实践

## 实践1：给指定用户配置权限

![flow](./assets/README-1629788341894.png)

目标：为blake用户授权default命名空间Pod读取权限 步骤：

1. 用kubernetes CA签发客户端证书
2. 生成kubeconfig授权文件
3. 创建RBAC权限策略

**用Kubernetes CA签发客户端证书（在Master上执行）**

关于cfssl

> CFSSL是CloudFlare开源的一款PKI/TLS工具。 CFSSL 包含一个命令行工具 和一个用于 签名，验证并且捆绑TLS证书的 HTTP API 服务。 使用Go语言编写。

参考：

- [https://www.jianshu.com/p/944f2003c829](https://www.jianshu.com/p/944f2003c829)
- [https://github.com/cloudflare/cfssl](https://github.com/cloudflare/cfssl)

安装cfssl

```shell
curl -s -L -o /bin/cfssl https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
curl -s -L -o /bin/cfssljson https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
curl -s -L -o /bin/cfssl-certinfo https://pkg.cfssl.org/R1.2/cfssl-certinfo_linux-amd64
chmod +x /bin/cfssl*
```

我已经下载好了，在k8s-study/chapter07/cfssl下

```shell
$ git clone https://oauth2:tmsHZgob-AQNA9C7-BD5@git.mysre.cn/jintang/k8s-study.git
```

解压、设置环境变量和修改可执行权限

```shell
$ mkdir -p /usr/local/cfssl
$ tar -xf k8s-study/chapter07/cfssl/cfssl.tar.gz -C /usr/local/cfssl
$ echo "export PATH=$PATH:/usr/local/cfssl" >> ~/.bash_profile
$ chmod +x /usr/local/cfssl
```

查看版本

```shell
$ cfssl version
------------------
Version: 1.2.0
Revision: dev
Runtime: go1.6
```

先创建目录

```shell
$ mkdir ca
$ cd ca
```

定义 ca-config.json 文件

```shell
cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "87600h"
    },
    "profiles": {
      "kubernetes": {
         "expiry": "87600h",
         "usages": [
            "signing",
            "key encipherment",
            "server auth",
            "client auth"
        ]
      }
    }
  }
}
EOF
```

定义 blake-csr.json 文件

```shell
cat > blake-csr.json <<EOF
{
  "CN": "blake",
  "hosts": [],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "L": "ShenZhen",
      "ST": "ShenZhen",
      "O": "k8s",
      "OU": "System"
    }
  ]
}
EOF
```

cfssl 生成证书

```shell
$ cfssl gencert -ca=/etc/kubernetes/pki/ca.crt -ca-key=/etc/kubernetes/pki/ca.key -config=ca-config.json -profile=kubernetes blake-csr.json | cfssljson -bare blake
```

查看生成的证书

```shell
$ ls -l
----------------
-rw-r--r-- 1 root root  993 8月  14 16:13 blake.csr
-rw-r--r-- 1 root root  218 8月  14 16:13 blake-csr.json
-rw------- 1 root root 1675 8月  14 16:13 blake-key.pem
-rw-r--r-- 1 root root 1277 8月  14 16:13 blake.pem
-rw-r--r-- 1 root root  294 8月  14 16:13 ca-config.json
```

到这一步，证书已经生成好了

**生成kubeconfig授权文件**

可以参考.kube/config文件有哪些组成部分，分为以下几步：

第一步 定义集群

```shell
$ kubectl config set-cluster kubernetes \
  --certificate-authority=/etc/kubernetes/pki/ca.crt \
  --embed-certs=true \
  --server=https://192.168.1.101:6443 \
  --kubeconfig=blake.kubeconfig
```

第二步：设置客户端认证用户

```shell
$ kubectl config set-credentials blake \
  --client-key=blake-key.pem \
  --client-certificate=blake.pem \
  --embed-certs=true \
  --kubeconfig=blake.kubeconfig
```

第三步：设置默认上下文

```shell
$ kubectl config set-context kubernetes \
--cluster=kubernetes \
--user=blake \
--kubeconfig=blake.kubeconfig
```

第四步：指定当前上下文

```shell
$ kubectl config use-context kubernetes --kubeconfig=blake.kubeconfig
```

到此，blake.kubeconfig已经生成好了，可以拷贝到node节点上进行验证

```shell
$ scp blake.kubeconfig root@node1:~
```

在node1上，先删掉原有的kubeconfig文件

```shell
$ rm -rf ~/.kube/config
$ export KUBECONFIG=""
```

验证 kubectl 是否可用

```shell
$ kubectl get po 
---------------------------
The connection to the server localhost:8080 was refused - did you specify the right host or port?
```

没有证书的情况下，会输出上述错误

然后通过 --kubeconfig 参数指定 kubeconfig文件

```shell
$ kubectl get po --kubeconfig=blake.kubeconfig
-------------------------------------------------
Error from server (Forbidden): pods is forbidden: User "blake" cannot list resource "pods" in API group "" in the namespace "default"
```

发现报错了，因为 blake 在"" API Group， 在 default 命名空间内，还没有权限去查看对应的 Pods 资源列表，此时就应该想到什么？那就是RBAC授权

**创建RBAC权限策略**

上述已经介绍过rbac的示例了，下面直接创建

定义 rbac.yaml 文件

```shell
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: default
  name: pod-reader
rules:
- apiGroups: [""]
  resources: ["pods", "services", "deployments"]
  verbs: ["get", "watch", "list"]

---

kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: read-pods
  namespace: default
subjects:
- kind: User
  name: blake
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```

创建

```shell
$ kubectl apply -f rbac.yaml
-----------------------------
role.rbac.authorization.k8s.io/pod-reader created
rolebinding.rbac.authorization.k8s.io/read-pods created
```

创建好了

在 node1 上重新验证

```shell
$ kubectl get po --kubeconfig=blake.kubeconfig
----------------------------------------------
NAME              READY   STATUS    RESTARTS   AGE
pod-configmap     1/1     Running   0          10h
secret-demo-pod   1/1     Running   0          9h
```

成功！

再试试查看 deployment 试试？

```shell
$ kubectl get deployments --kubeconfig=blake.kubeconfig
--------------------------------------------------------
Error from server (Forbidden): deployments.apps is forbidden: User "blake" cannot list resource "deployments" in API group "apps" in the namespace "default"
```

又发现没有权限，为什么？从提示 in API Group "apps" 得知，API Group不对。对比一下我们的rbac.yaml

```shell
...
rules:
- apiGroups: ["", "apps"] # 此处需要加上"apps"
  resources: ["pods", "services", "deployments"]
...
```

修改后，再次apply（注意在master上操作）

```shell
$ kubectl apply -f rbac.yaml
```

再试试查看 deployment 试试

```shell
$ kubectl get deployments --kubeconfig=blake.kubeconfig
```

切换成永久方式（个人喜好）

```shell
$ mv blake.kubeconfig ~/.kube/config
# 或者使用 $KUBECONFIG 方式指定
$ export $KUBECONFIG=~/blake.kubeconfig
```

更多的权限控制，可以参考官方文档 [RBAC权限控制](https://kubernetes.io/zh/docs/reference/access-authn-authz/rbac) , 上面都有很多的示例

**查看kubernetes-dashboard示例**

```shell
$ vim k8s-study/chatper02/yaml/kubernetes-dashboard.yaml
```

## 实践2：ConfigMap映射kdemo的配置文件

提供了向 Pod 注入配置数据的方法，用来将非机密性的数据保存到键值对中，比如存储卷中的配置文件。 ConfigMap 对象中存储的数据可以被 configMap 类型的卷引用，然后被 Pod 中运行的
容器化应用使用（以key-value的形式）。

您可以使用四种方式来使用 ConfigMap 配置 Pod 中的容器：

- 容器的环境变量
- 在只读卷里面添加一个文件，让应用来读取
- 编写代码在 Pod 中运行，使用 Kubernetes API 来读取 ConfigMap
- 容器 entrypoint 的命令行参数

这些不同的方法适用于不同的数据使用方式。

定义 kdemo-configmap.yaml

```shell
apiVersion: v1
kind: ConfigMap
metadata:
  name: kdemo-configmap
data:
  # 用于设置环境变量
  HOST: "localhost"
  PORT: "3306"
  USER: "root"
  MYSQL_ROOT_PASSWORD: "123456"
  
  # 用于映射配置文件
  params: |
    <?php

    return [
        'adminEmail' => 'blake@kdemo.com',
        'senderEmail' => 'noreply@kdemo.com',
        'senderName' => 'kdemo.com mailer',
    ];
```

查看 kdemo-configmap

```shell
$ kubectl describe configmap kdemo-configmap
```

kdemo 引用 configmap 配置, 定义 kdemo-deployment.yaml

```shell
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: kdemo
  name: kdemo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kdemo
  strategy: {}
  template:
    metadata:
      labels:
        app: kdemo
    spec:
      containers:
        - image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.11
          name: kdemo
          env:
            - name: MYSQL_DB_PASSWORD
              valueFrom:
                configMapKeyRef:
                  name: kdemo-configmap
                  key: MYSQL_ROOT_PASSWORD
          volumeMounts:
            - name: config
              mountPath: "/webser/www/kdemo/config/params.php"
              subPath: "params.php"
              readOnly: true
      volumes:
        - name: config
          configMap:
            name: kdemo-configmap
            items:
              - key: "params"
                path: "params.php"
---
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: kdemo
  name: kdemo
spec:
  ports:
    - port: 80
      protocol: TCP
      targetPort: 80
  selector:
    app: kdemo
  type: NodePort
status:
  loadBalancer: {}
```

注意 spec.containers.env 适用于指定容器的环境变量，其中 valueFrom 指明来源，configMapKeyRef 表示引用 configmap，找到对应的值后，在pod启动时会注入到环境变量中

volumeMounts和volumes，前面章节介绍过，是用于声明数据卷，是成对出现。

查看并进入容器

```shell
$ kubectl get po,svc
---------------------------------------------------------
NAME                         READY   STATUS    RESTARTS   AGE
pod/kdemo-7846975cb4-5g9gg   1/1     Running   0          116s

NAME                 TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
service/kdemo        NodePort    10.1.70.241   <none>        80:31076/TCP   5m20s
service/kubernetes   ClusterIP   10.1.0.1      <none>        443/TCP        8d

$ kubectl exec -it kdemo-7846975cb4-5g9gg -- bash
```

查看容器的环境变量

```shell
root@kdemo-85f7bf84dd-npgwj:/webser/www/kdemo# env | grep MYSQL
MYSQL_DB_PASSWORD=123456
```

查看配置文件 /webser/www/kdemo/config/params.php

```shell
$ cat config/params.php
------------------------
<?php

return [
    'adminEmail' => 'blake@kdemo.com',
    'senderEmail' => 'noreply@kdemo.com',
    'senderName' => 'kdemo.com mailer',
];
```

可以看到就是 configmap 中的内容

浏览器访问：http://192.168.1.101:31076?r=site/contact

## 实践3：Secret秘钥的应用

Secret 是一种包含少量敏感信息例如密码、令牌或密钥的对象。 这样的信息可能会被放在 Pod yaml中或者镜像中。 用户可以创建 Secret，同时系统也创建了一些 Secret。

要使用 Secret，Pod 需要引用 Secret。 Pod 可以用三种方式之一来使用 Secret：

- 作为挂载到一个或多个容器上的 卷 中的文件。
- 作为容器的环境变量
- 由 kubelet 在为 Pod 拉取镜像时使用

secret中的值，是必须经过base64编码的

```shell
$ echo "admin" | base64
-----------------------
YWRtaW4K

$ echo "123456" | base64
-----------------------
MTIzNDU2Cg==
```

定义 secret.yaml

```shell
apiVersion: v1
kind: Secret
metadata:
  name: db-user-pass
type: Opapque
data:
  username: YWRtaW4K
  password: MTIzNDU2Cg==
```

```shell
$ kubectl apply -f secret.yaml
```

使用命令来生成

```shell
$ cat > blake.com.key <<EOF
-----BEGIN RSA PRIVATE KEY-----
...
MIIEogIBAAKCAQEAlWU3hzL1QNq1sfFvFsr1j01D8YfsVaxDCF3iAWUMMy/kTl8y
G+Ek5EEkhHHO08CnE2kyaYhJUx9Bf2uwL2/F1fW/jU4PtZkthn1Lsd3oBFLpWA6G
...
-----END RSA PRIVATE KEY-----
EOF 

$ cat > blake.com.pem <<EOF
-----BEGIN CERTIFICATE-----
...
MIIEqjCCA5KgAwIBAgIQAnmsRYvBskWr+YBTzSybsTANBgkqhkiG9w0BAQsFADBh
MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3
...
-----END CERTIFICATE-----
EOF
```

```shell
$ kubectl create secret generic blake-ca \
  --from-file=blakekey=./blake.com.key \
  --from-file=blakepem=./blake.com.pem
```

查看 db-user-pass secret

```shell
$ kubectl get secret
-----------------------------------------------------------------------
NAME                  TYPE                                  DATA   AGE
blake-ca              Opaque                                2      5s
db-user-pass          Opapque                               2      12h

$ kubectl describe secret db-user-pass
------------------------------------------------------------------------
Name:         db-user-pass
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opapque

Data
====
password:  7 bytes
username:  6 bytes
```

可以看到内容时不展示的，只看到字节大小

使用 Pod 引用 Secret， 定义 pod-secret.yaml

```shell
apiVersion: v1
kind: Pod
metadata:
  name: secret-pod
spec:
  containers:
    - name: demo
      image: nginx:1.16
      env:
        - name: USER
          valueFrom:
            secretKeyRef:
              name: db-user-pass
              key: username
        - name: PASS
          valueFrom:
            secretKeyRef:
              name: db-user-pass
              key: password
      volumeMounts:
        - name: config
          mountPath: "/etc/nginx/ca/"
          readOnly: true
  volumes:
    - name: config
      secret:
        secretName: blake-ca
        items:
          - key: blakekey
            path: blake.com.key
          - key: blakepem
            path: blake.com.pem
```

查看 pod

```shell
$ kubectl get po
NAME                     READY   STATUS    RESTARTS   AGE
secret-pod               1/1     Running   0          72s
```

进入容器 secret-pod

```shell
$ kubectl exec -it secret-pod -- bash
```

查看环境变量

```shell
root@secret-pod:/# env | grep -e "USER" -e "PASS"
USER=admin
PASS=123456
```

查看证书

```shell
root@secret-pod:/# ls -l /etc/nginx/ca
total 0
lrwxrwxrwx 1 root root 20 Aug  8 01:52 blake.com.key -> ..data/blake.com.key
lrwxrwxrwx 1 root root 20 Aug  8 01:52 blake.com.pem -> ..data/blake.com.pem
```

```shell
root@secret-pod:/# cat /etc/nginx/ca/blake.com.key
-----BEGIN RSA PRIVATE KEY-----
...
MIIEogIBAAKCAQEAlWU3hzL1QNq1sfFvFsr1j01D8YfsVaxDCF3iAWUMMy/kTl8y
G+Ek5EEkhHHO08CnE2kyaYhJUx9Bf2uwL2/F1fW/jU4PtZkthn1Lsd3oBFLpWA6G
...
-----END RSA PRIVATE KEY-----
```

有了上面的应用，可以很轻松配置nginx的证书，不必担心证书存放的问题

# 四、思考

Kubernetes的RBAC权限控制中的主体subject，为什么要分为User、Group和ServiceAccount呢？他们分别的使用场景是怎么样的？

# 五、作业

1. 按照实践1，给自己分配一个kubeconfig文件
2. 利用statefulset启动一个mysql，把连接信息通过configmap存储，并把连接信息映射到kdemo的容器环境变量中