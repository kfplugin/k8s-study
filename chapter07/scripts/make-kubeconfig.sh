# !/bin/sh

# 定义集群
kubectl config set-cluster kubernetes \
  --certificate-authority=/etc/kubernetes/pki/ca.crt \
  --embed-certs=true \
  --server=https://192.168.1.101:6443 \
  --kubeconfig=blake.kubeconfig

# 设置客户端认证用户
kubectl config set-credentials blake \
  --client-key=blake-key.pem \
  --client-certificate=blake.pem \
  --embed-certs=true \
  --kubeconfig=blake.kubeconfig

# 设置默认上下文
kubectl config set-context kubernetes \
--cluster=kubernetes \
--user=blake \
--kubeconfig=blake.kubeconfig

# 指定当前上下文
kubectl config use-context kubernetes --kubeconfig=blake.kubeconfig