

```
#: 注释
$: 可执行命令
其他: 终端输出
```

## 一、ReplicaSet控制Pod副本数实践

<img src="./assets/replicaset.png" width=640 />

### Replication Controller(RC)

RC 的作用是声明 Pod 的副本数量在任意时刻都符合某个预期值，所以 RC 的定义包括如下几个部分。

- Pod 期待的副本数量（replicas）。
- 用于筛选目标 Pod 的 Label Selector。
- 当 Pod 的副本数量小于预期数量时，用于创建新 Pod 的 Pod 模板（template）。

当我们定义了一个 RC 并提交到 Kubernetes 集群中后，Master 节点上的 Controller Manager 组件就得到通知， 定期巡检系统中当前存活的目标 Pod，并确保目标Pod实例的数量刚好等于此 RC 的期望值。如果有过多的 Pod 副本在运行，系统就会停掉多余的 Pod；如果运行的 Pod 副本少于期望值，即如果某个 Pod 挂掉，系统就会自动创建新的 Pod 以保证数量等于期望值。



### ReplicaSet(RS)

RS与RC当前存在的唯一区别是：RC 只支持单个 Label 的等式，而 RS 中的 Label Selector 支持 `matchLabels` 和 `matchExpressions` 两种形式：

在 spec 下面描述 ReplicaSet 的基本信息，其中包含3个主要内容：

- replicas：表示期望的 Pod 的副本数量
- selector：Label Selector，用来匹配要控制的 Pod 标签，需要和下面的 Pod 模板中的标签一致
- template：Pod 模板，实际上就是以前我们定义的 Pod 内容，相当于把一个 Pod 的描述以模板的形式嵌入到了 ReplicaSet 中来。

### 部署ReplicaSet

`replicaSet.yaml`

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: kdemo-0817-replicaset
  labels:
    app: kdemo-0817-replicaset
spec:
  replicas: 3
  selector:
    matchLabels:
      tier: backend
    matchExpressions:
      - key: app
        operator: In
        values: ["kdemo-0817-replicaset"]
  template:
    metadata:
      labels:
        app: kdemo-0817-replicaset
        tier: backend
    spec:
      containers:
      - name: kdemo-0817-replicaset
        image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
```

```shell
$ kubectl apply -f `replicaSet.yaml`
replicaset.apps/kdemo-0817-replicaset created

$ kubectl get rs kdemo-0817-replicaset
NAME                    DESIRED   CURRENT   READY   AGE
kdemo-0817-replicaset   3         3         3       25s

$ kubectl get pods -l app=kdemo-0817-replicaset
NAME                          READY   STATUS    RESTARTS   AGE
kdemo-0817-replicaset-czgc8   1/1     Running   0          68s
kdemo-0817-replicaset-jxv9m   1/1     Running   0          68s
kdemo-0817-replicaset-p6p7r   1/1     Running   0          68s
```

这 3 个 Pod 就是我们在 RS 中声明的 3 个副本，现在我们删除其中一个 Pod

```shell
$ kubectl delete pod kdemo-0817-replicaset-p6p7r
pod "kdemo-0817-replicaset-p6p7r" deleted

$ kubectl get pods -l app=kdemo-0817-replicaset
NAME                          READY   STATUS    RESTARTS   AGE
kdemo-0817-replicaset-cvj95   1/1     Running   0          52s
kdemo-0817-replicaset-czgc8   1/1     Running   0          3m26s
kdemo-0817-replicaset-jxv9m   1/1     Running   0          3m26s

$ kubectl describe rs kdemo-0817-replicaset
Name:         kdemo-0817-replicaset
Namespace:    default
Selector:     app in (kdemo-0817-replicaset),tier=backend
Labels:       app=kdemo-0817-replicaset
Annotations:  <none>
Replicas:     3 current / 3 desired
Pods Status:  3 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=kdemo-0817-replicaset
           tier=backend
  Containers:
   kdemo-0817-replicaset:
    Image:        registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age    From                   Message
  ----    ------            ----   ----                   -------
  Normal  SuccessfulCreate  4m16s  replicaset-controller  Created pod: kdemo-0817-replicaset-jxv9m
  Normal  SuccessfulCreate  4m16s  replicaset-controller  Created pod: kdemo-0817-replicaset-czgc8
  Normal  SuccessfulCreate  4m16s  replicaset-controller  Created pod: kdemo-0817-replicaset-p6p7r
  Normal  SuccessfulCreate  102s   replicaset-controller  Created pod: kdemo-0817-replicaset-cvj95
```

可以发现最开始通过 ReplicaSet 控制器创建了 3 个 Pod，后面我们删除了 Pod 后， ReplicaSet 控制器又为我们创建了一个 Pod。

但是在实际应用上我们也不会直接使用 ReplicaSet，而是使用更上层的类似于 Deployment 资源对象。

## 二、Deployment实践

<img src="./assets/deployment.png" width=640 />

Deployment 是一个用来管理 ReplicaSet 的更高级概念，某种程度上我们不应该操作 ReplicaSet，而是直接使用 Deployment。Deployment 拥有 Rollout & Rollback 功能，方便我们管理。

`deployment.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kdemo-0817-deploy
  labels:
    app: kdemo-0817-deploy
spec:
  replicas: 3
  selector:
    matchLabels:
      tier: backend
    matchExpressions:
      - key: app
        operator: In
        values: ["kdemo-0817-deploy"]
  template:
    metadata:
      labels:
        app: kdemo-0817-deploy
        tier: backend
    spec:
      containers:
      - name: kdemo-0817-deploy
        image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5
```

我们这里只是将类型替换成了 Deployment，我们可以先来创建下这个资源对象：

```shell
$ kubectl apply -f deployment.yaml
deployment.apps/kdemo-0817-deploy created
```

查看 Pod 状态

```shell
$ kubectl get pods -l app=kdemo-0817-deploy
NAME                                 READY   STATUS    RESTARTS   AGE
kdemo-0817-deploy-6b9f5ff875-wmzg7   1/1     Running   0          3m33s
kdemo-0817-deploy-6b9f5ff875-xmz92   1/1     Running   0          3m33s
kdemo-0817-deploy-6b9f5ff875-zkbcb   1/1     Running   0          3m33s
```

和之前的 RS 对象是否没有什么两样，都是根据`spec.replicas`来维持的副本数量

看下其中一个Pod 的信息

```
$ kubectl describe pod kdemo-0817-deploy-6b9f5ff875-zkbcb
Name:         kdemo-0817-deploy-6b9f5ff875-zkbcb
Namespace:    default
Priority:     0
Node:         vm-k8s-node-2/192.168.64.32
Start Time:   Mon, 16 Aug 2021 23:42:56 -0400
Labels:       app=kdemo-0817-deploy
              pod-template-hash=6b9f5ff875
              tier=backend
Annotations:  cni.projectcalico.org/containerID: 3a5130d04737c1498af2af7a4a8845b3a15f9a9f1ec3693ad3bf48096590e8c4
              cni.projectcalico.org/podIP: 10.244.13.106/32
              cni.projectcalico.org/podIPs: 10.244.13.106/32
Status:       Running
IP:           10.244.13.106
IPs:
  IP:           10.244.13.106
Controlled By:  ReplicaSet/kdemo-0817-deploy-6b9f5ff875
...
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  7m9s  default-scheduler  Successfully assigned default/kdemo-0817-deploy-6b9f5ff875-zkbcb to vm-k8s-node-2
  Normal  Pulled     7m5s  kubelet            Container image "registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.5" already present on machine
  Normal  Created    7m5s  kubelet            Created container kdemo-0817-deploy
  Normal  Started    7m4s  kubelet            Started container kdemo-0817-deploy
```

Controlled By:  ReplicaSet/kdemo-0817-deploy-6b9f5ff875

再看一看ReplicaSet的信息

```shell
$ kubectl describe rs kdemo-0817-deploy-6b9f5ff875
Name:           kdemo-0817-deploy-6b9f5ff875
Namespace:      default
Selector:       app in (kdemo-0817-deploy),pod-template-hash=6b9f5ff875,tier=backend
Labels:         app=kdemo-0817-deploy
                pod-template-hash=6b9f5ff875
                tier=backend
Annotations:    deployment.kubernetes.io/desired-replicas: 3
                deployment.kubernetes.io/max-replicas: 4
                deployment.kubernetes.io/revision: 1
Controlled By:  Deployment/kdemo-0817-deploy
...
Events:
  Type    Reason            Age    From                   Message
  ----    ------            ----   ----                   -------
  Normal  SuccessfulCreate  9m40s  replicaset-controller  Created pod: kdemo-0817-deploy-6b9f5ff875-zkbcb
  Normal  SuccessfulCreate  9m40s  replicaset-controller  Created pod: kdemo-0817-deploy-6b9f5ff875-wmzg7
  Normal  SuccessfulCreate  9m40s  replicaset-controller  Created pod: kdemo-0817-deploy-6b9f5ff875-xmz92
  Normal  SuccessfulCreate  8m6s   replicaset-controller  Created pod: kdemo-0817-deploy-6b9f5ff875-5rz4d
  Normal  SuccessfulDelete  6m42s  replicaset-controller  Deleted pod: kdemo-0817-deploy-6b9f5ff875-5rz4d
```

Controlled By:  Deployment/kdemo-0817-deploy

通过实践我们可以清楚的看到 Deployment 与 ReplicaSet 和 Pod 的关系，就是一层一层进行控制的。ReplicaSet 作用和之前一样还是来保证 Pod 的个数始终保存指定的数量，而 Deployment 是通过管理 ReplicaSet 的数量和属性来实现`水平扩展/收缩`以及`滚动更新`两个功能的。

#### 水平伸缩
水平伸缩的功能比较简单，因为 ReplicaSet 就可以实现，所以 Deployment 控制器只需要去修改它缩控制的 ReplicaSet 的 Pod 副本数量就可以了。我们可以使用 kubectl scale 命令来完成这个操作

```
$ kubectl scale deployment kdemo-0817-deploy --replicas=4
deployment.apps/kdemo-0817-deploy scaled
```

扩展完成后可以查看当前的 RS 对象

```shell
$ kubectl get rs -l app=kdemo-0817-deploy
NAME                           DESIRED   CURRENT   READY   AGE
kdemo-0817-deploy-6b9f5ff875   4         4         4       4m17s
```

Pod 数量已经变成 4 了

#### 滚动更新

`deployment-RollingUpdate.yaml`

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kdemo-0817-deploy
  labels:
    app: kdemo-0817-deploy
spec:
  replicas: 4
  selector:
    matchLabels:
      tier: backend
    matchExpressions:
      - key: app
        operator: In
        values: ["kdemo-0817-deploy"]
  minReadySeconds: 5
  strategy:  
    type: RollingUpdate  # 指定更新策略：RollingUpdate和Recreate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  template:
    metadata:
      labels:
        app: kdemo-0817-deploy
        tier: backend
    spec:
      containers:
      - name: kdemo-0817-deploy
        image: registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.8
```

与前面的`deployment.yaml`相比较，我们指定了更新策略并更改了镜像版本：

```shell
  minReadySeconds: 5
  strategy:  
    type: RollingUpdate  # 指定更新策略：RollingUpdate和Recreate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
```

- `minReadySeconds`：表示 Kubernetes 在等待设置的时间后才进行升级，如果没有设置该值，Kubernetes 会假设该容器启动起来后就提供服务了，默认值就是0。
- `type: RollingUpdate`：表示设置更新策略为滚动更新，可以设置为`Recreate`和`RollingUpdate`两个值，`Recreate`表示全部重新创建，默认值就是`RollingUpdate`。
- `maxSurge`：表示升级过程中最多可以比原先设置多出的 Pod 数量，例如：`maxSurage=1，replicas=4`，就表示Kubernetes 会先启动一个新的 Pod，然后才删掉一个旧的 Pod。
- `maxUnavaible`：表示升级过程中最多有多少个 Pod 处于无法提供服务的状态，当`maxSurge`不为0时，该值也不能为0，例如：`maxUnavaible=1`，则表示 Kubernetes 整个升级过程中最多会有1个 Pod 处于无法服务的状态。

```shell
$ kubectl apply -f deployment-RollingUpdate.yaml
deployment.apps/kdemo-0817-deploy configured

# 观察Pods的状态变化
$ kubectl get pods -l app=kdemo-0817-deploy -w
NAME                                 READY   STATUS    RESTARTS   AGE
kdemo-0817-deploy-764d955b8d-6pcnn   1/1     Running   0          4m58s
kdemo-0817-deploy-764d955b8d-6x8qg   1/1     Running   0          5m7s
kdemo-0817-deploy-764d955b8d-rmjb4   1/1     Running   0          4m58s
kdemo-0817-deploy-764d955b8d-snlft   1/1     Running   0          5m7s
kdemo-0817-deploy-6b9f5ff875-2nl7b   0/1     Pending   0          0s
kdemo-0817-deploy-764d955b8d-rmjb4   1/1     Terminating   0          5m32s
kdemo-0817-deploy-6b9f5ff875-2nl7b   0/1     Pending       0          1s
kdemo-0817-deploy-6b9f5ff875-2nl7b   0/1     ContainerCreating   0          1s
kdemo-0817-deploy-6b9f5ff875-g4j5n   0/1     Pending             0          0s
kdemo-0817-deploy-6b9f5ff875-g4j5n   0/1     Pending             0          0s
kdemo-0817-deploy-6b9f5ff875-g4j5n   0/1     ContainerCreating   0          0s
kdemo-0817-deploy-6b9f5ff875-g4j5n   0/1     ContainerCreating   0          1s
kdemo-0817-deploy-6b9f5ff875-2nl7b   0/1     ContainerCreating   0          3s
kdemo-0817-deploy-6b9f5ff875-g4j5n   1/1     Running             0          4s
kdemo-0817-deploy-6b9f5ff875-2nl7b   1/1     Running             0          5s
kdemo-0817-deploy-764d955b8d-6x8qg   1/1     Terminating         0          5m50s
kdemo-0817-deploy-764d955b8d-6pcnn   1/1     Terminating         0          5m41s
kdemo-0817-deploy-6b9f5ff875-bnsbd   0/1     Pending             0          0s
kdemo-0817-deploy-6b9f5ff875-bnsbd   0/1     Pending             0          0s
kdemo-0817-deploy-6b9f5ff875-bb7hw   0/1     Pending             0          0s
kdemo-0817-deploy-6b9f5ff875-bb7hw   0/1     Pending             0          0s
kdemo-0817-deploy-6b9f5ff875-bnsbd   0/1     ContainerCreating   0          0s
kdemo-0817-deploy-6b9f5ff875-bb7hw   0/1     ContainerCreating   0          1s
kdemo-0817-deploy-6b9f5ff875-bnsbd   0/1     ContainerCreating   0          3s
kdemo-0817-deploy-6b9f5ff875-bb7hw   0/1     ContainerCreating   0          3s
kdemo-0817-deploy-6b9f5ff875-bb7hw   1/1     Running             0          4s
kdemo-0817-deploy-6b9f5ff875-bnsbd   1/1     Running             0          5s
kdemo-0817-deploy-764d955b8d-snlft   1/1     Terminating         0          6m
kdemo-0817-deploy-764d955b8d-rmjb4   1/1     Terminating         0          6m2s
kdemo-0817-deploy-764d955b8d-rmjb4   0/1     Terminating         0          6m4s
kdemo-0817-deploy-764d955b8d-rmjb4   0/1     Terminating         0          6m12s
kdemo-0817-deploy-764d955b8d-rmjb4   0/1     Terminating         0          6m12s
kdemo-0817-deploy-764d955b8d-6x8qg   1/1     Terminating         0          6m21s
kdemo-0817-deploy-764d955b8d-6pcnn   1/1     Terminating         0          6m12s
kdemo-0817-deploy-764d955b8d-6x8qg   0/1     Terminating         0          6m22s
kdemo-0817-deploy-764d955b8d-6pcnn   0/1     Terminating         0          6m14s
kdemo-0817-deploy-764d955b8d-6x8qg   0/1     Terminating         0          6m30s
kdemo-0817-deploy-764d955b8d-6x8qg   0/1     Terminating         0          6m30s
kdemo-0817-deploy-764d955b8d-snlft   1/1     Terminating         0          6m30s
kdemo-0817-deploy-764d955b8d-6pcnn   0/1     Terminating         0          6m22s
kdemo-0817-deploy-764d955b8d-6pcnn   0/1     Terminating         0          6m22s
kdemo-0817-deploy-764d955b8d-snlft   0/1     Terminating         0          6m32s
kdemo-0817-deploy-764d955b8d-snlft   0/1     Terminating         0          6m33s
kdemo-0817-deploy-764d955b8d-snlft   0/1     Terminating         0          6m33s
```



#### 回滚 

查看更新版本

```shell
$ kubectl rollout history deployment kdemo-0817-deploy
deployment.apps/kdemo-0817-deploy
REVISION  CHANGE-CAUSE
2         <none>
3         <none>
```

查看指定版本信息

```shell
$ kubectl rollout history deployment kdemo-0817-deploy --revision=1
```

回滚到指定版本

```shell
$ kubectl rollout undo deployment kdemo-0817-deploy --to-revision=1
deployment.apps/kdemo-0817-deploy rolled back
```

查看回滚状态

```shell
$ kubectl rollout status deployment kdemo-0817-deploy
Waiting for deployment "kdemo-0817-deploy" rollout to finish: 2 out of 4 new replicas have been updated...
Waiting for deployment "kdemo-0817-deploy" rollout to finish: 2 out of 4 new replicas have been updated...
Waiting for deployment "kdemo-0817-deploy" rollout to finish: 2 out of 4 new replicas have been updated...
Waiting for deployment "kdemo-0817-deploy" rollout to finish: 2 out of 4 new replicas have been updated...
Waiting for deployment "kdemo-0817-deploy" rollout to finish: 1 old replicas are pending termination...
Waiting for deployment "kdemo-0817-deploy" rollout to finish: 1 old replicas are pending termination...
Waiting for deployment "kdemo-0817-deploy" rollout to finish: 1 old replicas are pending termination...
Waiting for deployment "kdemo-0817-deploy" rollout to finish: 1 old replicas are pending termination...
deployment "kdemo-0817-deploy" successfully rolled out
```

#### 暂停 

```shell
$ kubectl rollout pause deployment kdemo-0817-deploy
```

#### 恢复

```shell
$ kubectl rollout resume  deployment kdemo-0817-deploy
```



## 三、使用StatefulSet创建有状态的应用

<img src="./assets/statefulset.png" alt="statefulset" width=640 />

#### 特点

`StatefulSet` 有以下几个特点

- 稳定，并且具有唯一的网络标识。
- 稳定，并且可持久存储。
- 有序，可优雅的部署和伸缩。
- 有序，可优雅的删除和停止。
- 有序，可自动的滚更新。

也就是说如果我们的应用如有以上的任何一个特点我们就可以使用StatefulSet控制器来完成应用的部署。



#### 网络标识

- Pod名：唯一且不会发生变化，由 $(statefulset name)-(0-N)组成，N为一个无限大的正整数，从0开始。
- 容器主机名：与Pod名称一至且不会发生变化。
- A记录：每一个副本都拥有唯一且不变的一条A记录指定自己，格式为：$(pod name).$(service name).$(namespace name).svc。
- Pod标签，通过StatefulSet创建的每个Pod都拥有一个唯一的标签，格式为：statefulset.kubernetes.io/pod-name=$(pod name)，通常可以将新的service单独关联到此标签，来解决某个Pod的问题等。



#### 存储

每个Pod都对应一个PVC，PVC的名称格式：$(volumeClaimTemplates name)-$(pod name)-(0-N)，N为一个无限大的正整数，从0开始。

当Pod被删除的时候不会自动删除对应的PVC，需要手动删除。
每次Pod升级，重启，删除重新创建后都呆以保证使用的是首次使用的PVC，保证数据的唯一性与持久化。



#### 有序

- 当部署有N个副本的StatefulSet时候，严格按照从0到N的顺序创建，并且下一个Pod创建的前提是上一个Pod已经Running状态。
- 当删除有N个副本的StatefulSet时候，严格按照从N到0的顺序删除，并且下一个Pod删除的前提是上一个Pod已经完全Delete。
- 当扩容StatefulSet副本的时候，每增加一个pod前提是上一个Pod已经Running状态。
- 当减少StatefulSet副本的时候，每删除一个pod前提是上一个Pod已经完全Delete。
- 当升级StatefulSet的时候，严格按照从N到1的顺序升级，并且下一个Pod升级的前提是上一个Pod已经Running状态。



#### 无状态服务(Stateless Service)

该服务运行的实例不会在本地存储需要持久化的数据，并且多个实例对于同一个请求响应的结果是完全一致的，比如kdemo，我们可以同时启动多个实例，但是我们访问任意一个实例得到的结果都是一样的，因为他唯一需要持久化的数据是存储在MySQL数据库中的，所以我们可以说 kdemo 这个应用是无状态服务，但是 MySQL 数据库就不是了，因为他需要把数据持久化到本地。

#### 有状态服务(Stateful Service)

就和上面的概念是对立的了，该服务运行的实例需要在本地存储持久化数据，比如上面的 MySQL 数据库，你现在运行在节点 Node-1，那么他的数据就存储在Node-1上面的，如果这个时候你把该服务迁移到节点 Node-2 去的话，那么就没有之前的数据了，因为他需要去对应的数据目录里面恢复数据，而此时没有任何数据。

#### 无头服务(Headless Service)

`Headless Service` 在定义上和普通的 Service 几乎一致, 只是他的 `clusterIP: None`，所以，这个 Service 被创建后并不会被分配一个 cluster IP，而是会以 DNS 记录的方式暴露出它所代理的 Pod，而且还有一个非常重要的特性，对于 `Headless Service` 所代理的所有 Pod 的 IP 地址都会绑定一个如下所示的 DNS 记录：

```
<pod-name>.<svc-name>.<namespace>.svc.cluster.local
```

#### volumeClaimTemplates

用于给StatefulSet的Pod申请PVC，称为卷申请模板，它会为每个Pod生成不同的PVC，关绑定到PV，从而实现Pod的专有存储。

#### 部署NFS

部署nfs服务端到master节点

```
$ yum install nfs-utils

$ systemctl enable rpcbind
$ systemctl enable nfs

$ systemctl start rpcbind
$ systemctl start nfs

$ mkdir /data/nfs/pv-1
$ mkdir /data/nfs/pv-2
$ chmod -R 755 /data/nfs

$ vi /etc/exports
/data/nfs/pv-1     *(rw,sync,no_root_squash,no_all_squash)
/data/nfs/pv-2     *(rw,sync,no_root_squash,no_all_squash)
```

- `/data`: 共享目录位置。
- `192.168.0.0/24`: 客户端 IP 范围，`*` 代表所有，即没有限制。
- `rw`: 权限设置，可读可写。
- `sync`: 同步共享目录。
- `no_root_squash`: 可以使用 root 授权。
- `no_all_squash`: 可以使用普通用户授权。

```
$ systemctl restart nfs
```

worker节点安装nfs客户端

```
$ yum install nfs-utils
$ systemctl enable rpcbind
$ systemctl start rpcbind
```



我们先准备两个存储卷（PV）

`pv.yaml`

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-pv-1
spec:
  capacity:
    storage: 1Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: slow
  nfs:
    server: vm-k8s-master
    path: /data/nfs/pv-1
    
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-pv-2
spec:
  capacity:
    storage: 1Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: slow
  nfs:
    server: vm-k8s-master
    path: /data/nfs/pv-2
```



```shell
$ kubectl apply -f pv.yaml
persistentvolume/nfs-pv-1 created
persistentvolume/nfs-pv-2 created

$ kubectl get pv
NAME       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
nfs-pv-1   1Gi        RWO            Recycle          Available           slow                    3m11s
nfs-pv-2   1Gi        RWO            Recycle          Available           slow                    3m11s
```

可以看到成功创建了两个 PV 对象，状态是：`Available`。

定义一个Headless Service 

 `headless-svc.yaml`

```
apiVersion: v1
kind: Service
metadata:
  name: nginx
  namespace: default
  labels:
    app: nginx
spec:
  ports:
  - name: http
    port: 80
  clusterIP: None
  selector:
    app: nginx
```



`statefulSet.yaml`

```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
  namespace: default
spec:
  serviceName: "nginx"
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - name: web
          containerPort: 80
        volumeMounts:
        - name: www
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: www
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
      storageClassName: slow
```



```
$ kubectl apply -f headless-svc.yaml
service/nginx created

$ kubectl get service nginx
NAME    TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
nginx   ClusterIP   None         <none>        80/TCP    9s
```

创建对应的 StatefulSet 对象了：

```shell
$ kubectl apply -f statefulSet.yaml 
statefulset.apps/web created

$ kubectl get pvc
NAME        STATUS   VOLUME     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
www-web-0   Bound    nfs-pv-1   1Gi        RWO            slow           90s
www-web-1   Bound    nfs-pv-2   1Gi        RWO            slow           67s
```



```
$ kubectl run -it --image busybox:1.28.3 test --restart=Never --rm /bin/sh

$ / # nslookup web-0.nginx
```

通过 `Headless Service`，StatefulSet 就保证了 Pod 网络标识的唯一稳定性，由于 Pod IP 并不是固定的，所以我们访问`有状态应用`实例的时候，就必须使用 DNS 记录的方式来访问

最后我们可以通过删除 StatefulSet 对象来删除所有的 Pod，Pod是按照倒序的方式进行删除的

```
$ kubectl delete statefulsets web
statefulset.apps "web" deleted

$ kubectl get pods -w
```



## 四、DaemonSet运行后台任务

<img src="./assets/daemonset.png" alt="daemonset" width=640 />



DaemonSet 确保全部（或者某些）节点上运行一个 Pod 的副本。 当有节点加入集群时， 也会为他们新增一个 Pod 。 当有节点从集群移除时，这些 Pod 也会被回收。删除 DaemonSet 将会删除它创建的所有 Pod。

DaemonSet 的一些典型用法：

- 集群存储守护程序，如 glusterd、ceph 要部署在每个节点上以提供持久性存储；
- 节点监控守护进程，如 Prometheus 监控集群，可以在每个节点上运行一个 node-exporter 进程来收集监控节点的信息；
- 日志收集守护程序，如 ilog-tail、fluentd 或 logstash，在每个节点上运行以收集容器的日志
- 节点网络插件，比如 flannel、calico 在每个节点上运行为 Pod 提供网络服务。



这里需要特别说明的一个就是关于 DaemonSet 运行的 Pod 的调度问题，正常情况下，Pod 运行在哪个节点上是由 Kubernetes 的调度器策略来决定的，然而，由 DaemonSet 控制器创建的 Pod 实际上提前已经确定了在哪个节点上了（Pod创建时指定了`.spec.nodeName`），所以：

- DaemonSet 并不关心一个节点的`unshedulable`字段。
- DaemonSet 可以创建 Pod，即使调度器还没有启动。

在每个节点上部署一个 Nginx Pod：

`nginx-ds.yaml`

```
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: nginx-ds
  namespace: default
spec:
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx
        ports:
        - name: http
          containerPort: 80
```



```
$ kubectl apply -f nginx-ds.yaml
daemonset.apps/nginx-ds created
```



查看 Pod 的状态

```shell
$ kubectl get nodes
NAME            STATUS   ROLES                  AGE   VERSION
vm-k8s-master   Ready    control-plane,master   10d   v1.21.3
vm-k8s-node-1   Ready    <none>                 8d    v1.21.3
vm-k8s-node-2   Ready    <none>                 8d    v1.21.3

kubectl get pods -l app=nginx -o wide
NAME             READY   STATUS    RESTARTS   AGE     IP              NODE            NOMINATED NODE   READINESS GATES
nginx-ds-5fpx5   1/1     Running   0          2m32s   10.244.32.229   vm-k8s-node-1   <none>           <none>
nginx-ds-xbkxd   1/1     Running   0          2m32s   10.244.72.14    vm-k8s-master   <none>           <none>
nginx-ds-zvwq8   1/1     Running   0          2m32s   10.244.13.123   vm-k8s-node-2   <none>           <none>
```

集群中的 Pod 和 Node 是一一对应的，而 DaemonSet 会管理全部机器上的 Pod 副本，负责对它们进行更新和删除。

那么，DaemonSet 控制器是如何保证每个 Node 上有且只有一个被管理的 Pod 呢？

- 首先控制器从 Etcd 获取到所有的 Node 列表，然后遍历所有的 Node。
- 根据资源对象定义是否有调度相关的配置，然后分别检查 Node 是否符合要求。
- 在可运行 Pod 的节点上检查是否已有对应的 Pod，如果没有，则在这个 Node 上创建该 Pod；如果有，并且数量大于 1，那就把多余的 Pod 从这个节点上删除；如果有且只有一个 Pod，那就说明是正常情况。



## 五、Job实践

在日常的工作中经常都会遇到一些需要进行批量数据处理和分析的需求，当然也会有按时间来进行调度的工作，在我们的 Kubernetes 集群中为我们提供了 `Job` 和 `CronJob` 两种资源对象来应对我们的这种需求。

#### Job

我们用 `Job` 这个资源对象来创建一个倒计时的任务，对应的资源清单如下所示

`job.yaml`

```
apiVersion: batch/v1
kind: Job
metadata:
  name: job
spec:
  template:
    spec:
      restartPolicy: Never
      containers:
      - name: counter
        image: busybox
        command:
        - "bin/sh"
        - "-c"
        - "for i in 9 8 7 6 5 4 3 2 1; do echo $i; done"
```

我们可以看到 `Job` 中也是一个 Pod 模板，和 Deployment、StatefulSet 是一致的，只是 Pod 中的容器要求是一个任务，而不是一个常驻前台的进程了，因为需要退出，所以 `Job` 的 `RestartPolicy` 仅支持 `Never` 和 `OnFailure` 两种，不支持 `Always`。

```shell
$ kubectl apply -f job.yaml
job.batch/job created

$ kubectl get job 
NAME   COMPLETIONS   DURATION   AGE
job    1/1           25s        33s

$ kubectl get pods         
NAME                                 READY   STATUS      RESTARTS   AGE
job-c8s94                            0/1     Completed   0          41s

$ kubectl logs job-c8s94
9
8
7
6
5
4
3
2
1
```

`restartPolicy=Never`：任务在执行失败后 Job 控制器就会不断地尝试创建一个新 Pod，我们可以通过 Job 对象的 `spec.backoffLimit` 字段来定义重试次数，另外需要注意的是 Job 控制器重新创建 Pod 的间隔是呈指数增加的，即下一次重新创建 Pod 的动作会分别发生在 10s、20s、40s… 后。

 `restartPolicy=OnFailure`：任务执行失败后，Job 控制器就不会去尝试创建新的 Pod了，它会不断地尝试重启 Pod 里的容器。

上面我们这里的 Job 任务对应的 Pod 在运行结束后，会变成 `Completed` 状态，但是如果执行任务的 Pod 因为某种原因一直没有结束，我们可以在 Job 对象中通过设置字段 `spec.activeDeadlineSeconds` 来限制任务运行的最长时间：

```
spec:
  activeDeadlineSeconds: 100
```

那么当我们的任务 Pod 运行超过了 100s 后，这个 Job 的所有 Pod 都会被终止，并且， Pod 的终止原因会变成 `DeadlineExceeded`。



#### CronJob

`Job` 负责处理任务，即仅执行一次的任务，它保证批处理任务的一个或多个 Pod 成功结束。而`CronJob` 则就是在 `Job` 上加上了时间调度，和 Linux 中的 `crontab` 非常类似。

`cronjob.yaml`

```
apiVersion: batch/v1
kind: CronJob
metadata:
  name: cronjob
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          restartPolicy: OnFailure
          containers:
          - name: hello
            image: busybox
            args:
            - "bin/sh"
            - "-c"
            - "for i in 9 8 7 6 5 4 3 2 1; do echo $i; done"
```

`.spec.jobTemplate`：指定需要运行的任务，格式同`job`

`.spec.successfulJobsHistoryLimit`：指定可以保留多少完成的 `Job`，默认没有限制，所有成功的 `Job` 都会被保留。

`.spec.failedJobsHistoryLimit`：指定可以保留多少失败的 `Job`，默认没有限制，所有失败的 `Job` 都会被保留。

`.spec.ttlSecondsAfterFinished`：自动清理已结束的`Job`， **TTL 机制仍然是一种 Alpha 状态的功能特性，Kubernetes v1.21 [stable]** 

```shell
$ kubectl apply -f cronjob.yaml
cronjob.batch/cronjob created
```

然后可以查看对应的 Cronjob 资源对象

```shell
$ kubectl get cronjob
NAME      SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob   */1 * * * *   False     0        43s             2m38s
```

查看一下 Job 资源对象，这个就是因为上面我们设置的 CronJob 资源对象，每1分钟执行一个新的 Job

```shell
$ kubectl get job 
NAME                      COMPLETIONS   DURATION   AGE
cronjob-demo-1574147280   1/1           6s         2m45s
cronjob-demo-1574147340   1/1           11s        105s
cronjob-demo-1574147400   1/1           5s         45s
$ kubectl get pods
NAME                      READY   STATUS      RESTARTS   AGE
cronjob-demo-1574147340-ksd5x   0/1     Completed           0          3m7s
cronjob-demo-1574147400-pts94   0/1     Completed           0          2m7s
cronjob-demo-1574147460-t5hcd   0/1     Completed           0          67s
cronjob-demo-1574147520-vmjfr   0/1     ContainerCreating   0          7s
```



## 六、思考

你的Kubernetes集群中分别有哪些服务使用了Deployment、StatefulSet、DaemonSet、Job？对应的服务为什么要使用这几种资源？



## 七、作业

使用StatefulSet部署MySQL，并使用kdemo连接测试。

使用Job（cronjob）定时备份MySQL数据到虚拟机。



## 八、清理

此小节我们将对本次实践所涉及到的资源进行清理（如果有需要的话）

```shell
# cd到yaml文件的目录执行以下命令

kubectl delete -f replicaSet.yaml
kubectl delete -f deployment.yaml
kubectl delete -f deployment-RollingUpdate.yaml
kubectl delete -f headless-svc.yaml
kubectl delete -f statefulSet.yaml
kubectl delete -f nginx-ds.yaml
kubectl delete -f job.yaml
kubectl delete -f cronjob.yaml
kubectl delete pvc www-web-0
kubectl delete pvc www-web-1
kubectl delete -f pv.yaml 


```

