#!/bin/bash

# 基础环境配置

# 设置域名
cat >>/etc/hosts <<EOF
192.168.1.101 master
192.168.1.102 node1
192.168.1.103 node2
EOF

# 下载阿里源
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

# 安装基础软件包
yum install -y yum-utils device-mapper-persistent-data lvm2 wget net-tools ntp git

# 同步系统时间
ntpdate 0.asia.pool.ntp.org

# 开启br_netfilter
modprobe br_netfilter

# 关闭防火墙
systemctl stop firewalld
systemctl disable firewalld

# 关闭selinux
setenforce 0
sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config

# 关闭swap
swapoff -a
yes | cp /etc/fstab /etc/fstab_bak
sed -i "$(sed -n '/swap/=' /etc/fstab)s/^/#/g" /etc/fstab

# 网络配置
echo 1 >/proc/sys/net/ipv4/ip_forward
echo 1 >/proc/sys/net/bridge/bridge-nf-call-iptables