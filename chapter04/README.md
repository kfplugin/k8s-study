# 一、Service存在的意义

## 1. 访问Pod的两个问题

**问题1：Pod的IP并不固定**

解决办法：增加一个控制器，负责收集pod列表IP，然后动态地更新到负载均衡配置中

**问题2：Pod是可以有多个副本的，并且每个副本的ip也不一样**

解决办法：在多个Pod副本之上，增加增加一个负载均衡器

**实践1：利用service负载到两个不同的pod**

启动两个pod，分别使用两个不同版本的kdemo镜像

```shell
$ kubectl create deployment kdemo --replicas=3 --image=registry.cn-shenzhen.aliyuncs.com/jayzone/kdemo:1.8
```

暴露服务

```shell
$ kubectl expose deployment kdemo --port=80 --type=NodePort --target-port=80
```

查看各个pod的ip

```shell
$ kubectl get po -o wide

NAME                    READY   STATUS    RESTARTS   AGE   IP               NODE    NOMINATED NODE   READINESS GATES
kdemo-f479b7c94-ms9rh   1/1     Running   0          65s   10.244.104.35    node2   <none>           <none>
kdemo-f479b7c94-n2npm   1/1     Running   0          65s   10.244.166.133   node1   <none>           <none>
kdemo-f479b7c94-wk72q   1/1     Running   0          65s   10.244.166.132   node1   <none>           <none>
```

或者访问 [https://192.168.1.101:30001](https://192.168.1.101:30001), 在kubernetes-dashboard上查看pod的ip分配情况

可以看出，pod的启动会给它分配一个唯一的ip，尽管在不同的node上，分配的ip也是唯一的。

## 2. Service是如何找到pod？

![service负载](./assets/README-1628761669333.png)

查看pod，service，增加--show-labels参数

```shell
$ kubectl get po,svc -o wide --show-labels
```

![labels](./assets/README-1628790426261.png)

![label-yaml](./assets/README-1628790925596.png)

可以看到，service是通过selector字段（app=kdemo）指定的标签来找到带上app=kdemo标签的pod

利用-l 参数，模拟service查找pod的命令

```shell
$ kubectl get pod -o wide --show-labels -l app=kdemo

NAME                    READY   STATUS    RESTARTS   AGE   IP               NODE    NOMINATED NODE   READINESS GATES   LABELS
kdemo-f479b7c94-ms9rh   1/1     Running   0          14m   10.244.104.35    node2   <none>           <none>            app=kdemo,pod-template-hash=f479b7c94
kdemo-f479b7c94-n2npm   1/1     Running   0          14m   10.244.166.133   node1   <none>           <none>            app=kdemo,pod-template-hash=f479b7c94
kdemo-f479b7c94-wk72q   1/1     Running   0          14m   10.244.166.132   node1   <none>           <none>            app=kdemo,pod-template-hash=f479b7c94
```

只要这个 -l 参数指定的标签能找的的pod，就是service能够匹配到的pod

当service通过标签匹配到对应的pod时，会生成endpoint，可以通过下面命令查看

```shell
$ kubectl get endpoints
```

![ep](./assets/README-1628790690507.png)

## 3. Service的两个重要的功能

1. 服务发现：防止pod失联
2. 负载均衡：定义一组Pod的访问策略

# 二、对比Nginx负载均衡器

## 1. 四层负载均衡与七层负载均衡

![四七层负载](./assets/README-1628821271245.png)

**四层负载均衡**

所谓四层负载均衡，也就是主要通过报文中的目标地址和端口，再加上负载均衡设备设置的服务器选择方式，决定最终选择的内部服务器。

以常见的TCP为例，负载均衡设备在接收到第一个来自客户端的SYN 请求时，并对报文中目标IP地址进行修改(
改为后端服务器IP），直接转发给该服务器。TCP的连接建立，即三次握手是客户端和服务器直接建立的，负载均衡设备只是起到一个类似路由器的转发动作。在某些部署情况下，为保证服务器回包可以正确返回给负载均衡设备，在转发报文的同时可能还会对报文原来的源地址进行修改。

![四层负载](./assets/README-1628819433747.png)

**七层负载均衡**

所谓七层负载均衡，也称为“内容交换”，也就是主要通过报文中的真正有意义的应用层内容，再加上负载均衡设备设置的服务器选择方式，决定最终选择的内部服务器。

以常见的TCP为例，负载均衡设备如果要根据真正的应用层内容再选择服务器，只能先代理最终的服务器和客户端建立连接(三次握手)
后，才可能接受到客户端发送的真正应用层内容的报文，然后再根据该报文中的特定字段，再加上负载均衡设备设置的服务器选择方式，决定最终选择的内部服务器。负载均衡设备在这种情况下，更类似于一个代理服务器。负载均衡和前端的客户端以及后端的服务器会分别建立TCP连接。所以从这个技术原理上来看，七层负载均衡明显的对负载均衡设备的要求更高，处理七层的能力也必然会低于四层模式的部署方式。

![七层负载均衡](./assets/README-1628819607137.png)

**两种层级的负载均衡对比**

- 四层负载只有一次tcp连接，数据包只经过传输层，无需向上交付，也就没有拆包和装包的过程，并且在内核空间完成，仅需要修改目标IP地址和源IP地址即可完成负载转发，所以性能极好，实现简单基本不会出问题。
- 七层负载中，client需要与负载均衡器做连接，同时负载均衡器也需要反向与server做tcp连接，一共做两次独立的tcp连接，由于工作ISO的七层，报文经过传输层交付到应用层，需要做拆包和装包，另外数据需要从内核空间拷贝数据到用户空间，所以性能消耗比四层负载要大得多。但是由于经过应用层，扩展能力强，功能强大。

从上面的对比看来四层负载与七层负载最大的区别就是效率与功能的区别。

四层与七层负载均衡，参考：[https://www.cnblogs.com/kevingrace/p/6137881.html](https://www.cnblogs.com/kevingrace/p/6137881.html)

内核空间与用户空间，参考：[https://blog.csdn.net/tommy_wxie/article/details/17122923/](https://blog.csdn.net/tommy_wxie/article/details/17122923)

TCP/IP四层模型与OSI参考模型, 参考：[https://www.cnblogs.com/gdayq/p/5797645.html](https://www.cnblogs.com/gdayq/p/5797645.html)

## 2. nginx的负载均衡配置

七层负载的配置

```shell
upstream phpserver {
    server 192.168.2.3:80;
    server 192.168.2.4:80;
}
upstream htmlserver {
    server 192.168.2.1:80;
    server 192.168.2.2:80;
}

location / {
    listen 80;
    root  /usr/share/nginx/html;
    index  index.html index.htm;
    if ($request_uri ~*\.html$){
        proxy_pass http://htmlserver;
    }
    if ($request_uri~* \.php$){
        proxy_pass http://phpserver;
    }
}
}
```

四层负载的配置

```shell
stream {
    upstream lb {
        server 172.16.1.5:80 weight=5 max_fails=3 fail_timeout=30s;
        server 172.16.1.6:80 weight=5 max_fails=3 fail_timeout=30s;
    }
    server {
        listen 80;
        proxy_connect_timeout 3s;
        proxy_timeout 3s;
        proxy_pass lb;
    }
}
```

对比Service的配置

```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    run: pod-service
  name: pod-service
spec:
  ports:
    - port: 80        # service负载均衡器的端口，只能在集群内部访问，相当于nginx中的listen 80
      protocol: TCP
      targetPort: 80  # 容器中的端口，相当于 nginx中 upstream下server的端口
      nodePort: 30001 # 节点暴露的端口，提供集群外外部访问，可以指定，否则随机从 30000~32767 之间取值
  selector:
    app: pod
  type: NodePort
status:
  loadBalancer: { }
```

# 三、Service的三种类型

## 1. clusterIP

![clusterIP](./assets/README-1628824180108.png)

**ClusterIP**：默认，分配一个稳定的IP地址，只能在集群内部访问，同一个namespace下的所有pod

**实践2：利用clusterIP暴露服务**

使用clusterIP方式暴露kdemo服务

```shell
# 如果上面已经暴露过kdemo服务，需要删掉
$ kubectl delete svc kdemo

# 使用ClusterIP类型的Service暴露kdemo服务，注意不要加--type参数，默认就是ClusterIP
$ kubectl expose deployment kdemo --port=80 --target-port=80
```

查看clusterIP

```shell
$ kubectl get svc -o wide

NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE    SELECTOR
kdemo        ClusterIP   10.1.73.118   <none>        80/TCP    18s    app=kdemo
kubernetes   ClusterIP   10.1.0.1      <none>        443/TCP   7d1h   <none>
```

```shell
$ kubectl describe svc kdemo
-----------------------------
Name:              kdemo
Namespace:         default
Labels:            app=kdemo
Annotations:       <none>
Selector:          app=kdemo
Type:              ClusterIP
IP:                10.1.73.118
Port:              <unset>  80/TCP
TargetPort:        80/TCP
Endpoints:         10.244.104.35:80,10.244.166.132:80,10.244.166.133:80
Session Affinity:  None
Events:            <none>
```

clusterIP为10.1.73.118，此时可以在集群的任意节点访问到kdemo服务

在node2访问

```shell
$ curl 10.1.73.118?r=site/hello
---------------------------------------
我的IP是：10.244.166.132
```

注意这个 r=site/hello 页面需要更新我最新版本的kdemo:1.8才有

查看访问到的是哪个节点的哪一个pod

```shell
$ kubectl get pod -o wide
--------------------------
NAME                    READY   STATUS    RESTARTS   AGE    IP               NODE    NOMINATED NODE   READINESS GATES
kdemo-f479b7c94-ms9rh   1/1     Running   0          113m   10.244.104.35    node2   <none>           <none>
kdemo-f479b7c94-n2npm   1/1     Running   0          113m   10.244.166.133   node1   <none>           <none>
kdemo-f479b7c94-wk72q   1/1     Running   0          113m   10.244.166.132   node1   <none>           <none>
```

根据ip能得知，访问到的是node1的kdemo-f479b7c94-wk72q这个pod

然而这个ClusterIP在集群外的地方是无法访问的。

## 2. NodePort

![nodeport](./assets/README-1628868463917.png)

**NodePort**：在每个节点上启用一个端口来暴露应用服务，可以在集群外部访问，同时也会分配一个稳定的ClusterIP

访问方式：<集群任意节点IP>:<暴露的端口>

端口范围默认：30000~32767

**实践3：利用NodePort暴露服务**

```shell
$ kubectl expose deployment kdemo --port=80 --type=NodePort --target-port=80 --dry-run=client -o yaml > yaml/nodeport-svc.yaml
$ kubectl apply -f yaml/nodeport-svc.yaml
```

> 在第二节课的思考题中提到，为什么通过nodeport暴露的服务，可以使用任意节点IP+指定端口访问服务，此处给出了答案

## 3. LoadBalance

![localbalance](./assets/README-1628825422559.png)

**LoadBalance**：与NodePort类似，在每个节点上启用一个端口来暴露应用服务，与此同时，Kubernetes会请求底层云平台上的负载均衡器，将每个Node（nodeip:nodeport）作为后端添加进去。

LoadBalancer类型的Service一般和云厂商的LB（比如阿里云的SLB）结合一块儿使用，用于将集群内部的服务暴露到外网，云厂商的LoadBalancer会给用户分配一个IP,以后经过该IP的流量会转发到你的Service.

# 四、Service代理模式

Service到底是怎么工作的呢？实际上它是借用了linux的其他技术实现的负载均衡，它是由kube-proxy + iptable/ipvs 共同实现的。

## 1. userspace

![services-userspace-overview.png](./assets/README-1628826578926.png)

这种模式是Kubernetes平台自己实现的一种负载均衡，因为工作在用户空间，需要频繁与内核空间通信（需要配置iptables规则），效率低下，随后有了iptables方式，此方式基本不用，并且在kubernetes的v1.1版本以后，service的默认代理模式已经改成了iptables，userspace模式基本已经没有使用价值。所以我们不介绍此种模式了，感兴趣可以去kubernetes官网了解。

## 2. iptables（默认）

![services-iptables-overview.png](./assets/README-1628826628918.png)

- 首先由master节点中的ApiServer来控制kube-proxy，由kube-proxy监听Service规则匹配到的endpoint，动态地增加和移除endpoint。
- 当Service添加一个endpoint时，Service会利用iptables生成大量的iptables规则，并且发到它暴露的pod中
- 浏览器中访问Service的ClusterIP，根据iptables规则，把流量转发到后端的pod中

**实践4：使用iptables代理模式暴露一个服务，并且跟踪service生成的iptables记录**

暴露一个nginx服务

```shell
$ kubectl create deployment web --image=nginx:1.16
$ kubectl expose deployment web --port=80 --type=NodePort --target-port=8081
```

查看pod

```shell
$ kubectl get po,svc
---------------------
NAME                       READY   STATUS    RESTARTS   AGE     IP               NODE    NOMINATED NODE   READINESS GATES
pod/web-6bc4dfc596-w9lz9   1/1     Running   0          9m13s   10.244.166.134   node1   <none>           <none>

NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE     SELECTOR
service/kubernetes   ClusterIP   10.1.0.1       <none>        443/TCP        7d3h    <none>
service/web          NodePort    10.1.155.208   <none>        80:30528/TCP   8m12s   app=web
```

修改nginx的listen端口

```shell
$ kubectl exec -it web-6bc4dfc596-w9lz9 -- bash
# 在容器中执行
root@web-6bc4dfc596-w9lz9:/# sed -i 's/80/8081/g' /etc/nginx/conf.d/default.conf
root@web-6bc4dfc596-w9lz9:/# nginx -s reload
2021/08/06 19:30:24 [notice] 31#31: signal process started
```

此时可以在浏览器通过 http://<任意节点IP>:30528 访问到这个nginx服务

查看service中的endpoint

```shell
$ kubectl get ep web
--------------------
NAME   ENDPOINTS             AGE
web    10.244.166.134:8081   9m44s
```

利用iptables-save来查看iptables规则，在任意节点上执行

```shell
# 在node1
$ iptables-save
# 之后会发现有很多条的iptables规则

# 统计多少条
$ iptables-save | wc -l 
```

只看web service相关的iptables规则

```shell
$ iptables-save | grep web
-----------------------------
-A KUBE-NODEPORTS -p tcp -m comment --comment "default/web" -m tcp --dport 30528 -j KUBE-MARK-MASQ
-A KUBE-NODEPORTS -p tcp -m comment --comment "default/web" -m tcp --dport 30528 -j KUBE-SVC-LOLE4ISW44XBNF3G
-A KUBE-SEP-STAATQQGBYEBOTBP -s 10.244.166.134/32 -m comment --comment "default/web" -j KUBE-MARK-MASQ
-A KUBE-SEP-STAATQQGBYEBOTBP -p tcp -m comment --comment "default/web" -m tcp -j DNAT --to-destination 10.244.166.134:8081
-A KUBE-SERVICES ! -s 10.244.0.0/16 -d 10.1.155.208/32 -p tcp -m comment --comment "default/web cluster IP" -m tcp --dport 80 -j KUBE-MARK-MASQ
-A KUBE-SERVICES -d 10.1.155.208/32 -p tcp -m comment --comment "default/web cluster IP" -m tcp --dport 80 -j KUBE-SVC-LOLE4ISW44XBNF3G
-A KUBE-SVC-LOLE4ISW44XBNF3G -m comment --comment "default/web" -j KUBE-SEP-STAATQQGBYEBOTBP
```

**规则分析**

首先，访问service的两种方式：

**1. 集群之内，在node或者pod里，通过curl，访问ClusterIP:80端口，如这里的 curl 10.1.155.208:80**

此种方式，根据上述的iptables规则，可以看出来走的是这一条：

```shell
-A KUBE-SERVICES -d 10.1.155.208/32 -p tcp -m comment --comment "default/web cluster IP" -m tcp --dport 80 -j KUBE-SVC-LOLE4ISW44XBNF3G
```

**解读**：在KUBE-SERVICES这条chain中，添加了规则：目标ip为 10.1.155.208 ，协议为tcp，注释为"default/web cluster IP"，目标端口为80，-j为转发到
KUBE-SVC-LOLE4ISW44XBNF3G 这条规则 也就是说"当你访问 10.1.155.208:80 时，转发到 KUBE-SVC-LOLE4ISW44XBNF3G 这条规则"

查看 KUBE-SVC-LOLE4ISW44XBNF3G 这条chain的规则：

```shell
$ iptables-save | grep KUBE-SVC-LOLE4ISW44XBNF3G
------------------------------------------------
:KUBE-SVC-LOLE4ISW44XBNF3G - [0:0]
-A KUBE-NODEPORTS -p tcp -m comment --comment "default/web" -m tcp --dport 30528 -j KUBE-SVC-LOLE4ISW44XBNF3G
-A KUBE-SERVICES -d 10.1.155.208/32 -p tcp -m comment --comment "default/web cluster IP" -m tcp --dport 80 -j KUBE-SVC-LOLE4ISW44XBNF3G
# 是这一条，转发到了 KUBE-SEP-STAATQQGBYEBOTBP 
-A KUBE-SVC-LOLE4ISW44XBNF3G -m comment --comment "default/web" -j KUBE-SEP-STAATQQGBYEBOTBP
```

查看 KUBE-SEP-STAATQQGBYEBOTBP 这条chain的规则

```shell
$ iptables-save | grep KUBE-SEP-STAATQQGBYEBOTBP
------------------------------------------------
:KUBE-SEP-STAATQQGBYEBOTBP - [0:0]
-A KUBE-SEP-STAATQQGBYEBOTBP -s 10.244.166.134/32 -m comment --comment "default/web" -j KUBE-MARK-MASQ
# 走的是这一条规则
-A KUBE-SEP-STAATQQGBYEBOTBP -p tcp -m comment --comment "default/web" -m tcp -j DNAT --to-destination 10.244.166.134:8081
```

**解读**：在 KUBE-SEP-STAATQQGBYEBOTBP 这条链中，-j DNAT 转发到 --to-destination 10.244.166.134:8081，就是service匹配到的endpoint

下面是完整的iptables规则流程

```shell
-A KUBE-SERVICES -d 10.1.155.208/32 -p tcp -m comment --comment "default/web cluster IP" -m tcp --dport 80 -j KUBE-SVC-LOLE4ISW44XBNF3G
-A KUBE-SVC-LOLE4ISW44XBNF3G -m comment --comment "default/web" -j KUBE-SEP-STAATQQGBYEBOTB
-A KUBE-SEP-STAATQQGBYEBOTBP -p tcp -m comment --comment "default/web" -m tcp -j DNAT --to-destination 10.244.166.134:8081
```

**iptables如何实现负载均衡？**

修改 deployment 的副本数为3个

```shell
$ kubectl edit deployment web
-----------------------------
apiVersion: apps/v1
kind: Deployment
...
  replicas: 3 # 修改为3
  revisionHistoryLimit: 10
  selector:
    matchLabels:
...
```

再次查看iptables规则

```shell
$ iptables-save | grep web
---------------------------
# 会发现多了一些规则
...
-A KUBE-SERVICES -d 10.1.155.208/32 -p tcp -m comment --comment "default/web cluster IP" -m tcp --dport 80 -j KUBE-SVC-LOLE4ISW44XBNF3G
# 增加了两条规则
-A KUBE-SVC-LOLE4ISW44XBNF3G -m comment --comment "default/web" -m statistic --mode random --probability 0.33333333349 -j KUBE-SEP-FULL7QTRU64DS2HM
-A KUBE-SVC-LOLE4ISW44XBNF3G -m comment --comment "default/web" -m statistic --mode random --probability 0.50000000000 -j KUBE-SEP-STAATQQGBYEBOTBP
-A KUBE-SVC-LOLE4ISW44XBNF3G -m comment --comment "default/web" -j KUBE-SEP-XIBZPFX27VINZJPS
```

新增的两条规则，通过--mode random --probability xxx 可以知道其意义：

- 0.3333的概率转发到规则：KUBE-SEP-FULL7QTRU64DS2HM
- 0.5000的概率转发到规则：KUBE-SEP-STAATQQGBYEBOTBP
- 剩下转发到规则：KUBE-SEP-XIBZPFX27VINZJPS

再次按照转发的chain过滤iptables规则

```shell
$ iptables-save | grep -e "KUBE-SEP-FULL7QTRU64DS2HM" -e "KUBE-SEP-STAATQQGBYEBOTBP" -e "KUBE-SEP-XIBZPFX27VINZJPS"
-------------------------------------------------------------------------------------------------------------------
-A KUBE-SEP-FULL7QTRU64DS2HM -s 10.244.104.36/32 -m comment --comment "default/web" -j KUBE-MARK-MASQ
-A KUBE-SEP-FULL7QTRU64DS2HM -p tcp -m comment --comment "default/web" -m tcp -j DNAT --to-destination 10.244.104.36:8081
-A KUBE-SEP-STAATQQGBYEBOTBP -s 10.244.166.134/32 -m comment --comment "default/web" -j KUBE-MARK-MASQ
-A KUBE-SEP-STAATQQGBYEBOTBP -p tcp -m comment --comment "default/web" -m tcp -j DNAT --to-destination 10.244.166.134:8081
-A KUBE-SEP-XIBZPFX27VINZJPS -s 10.244.166.136/32 -m comment --comment "default/web" -j KUBE-MARK-MASQ
-A KUBE-SEP-XIBZPFX27VINZJPS -p tcp -m comment --comment "default/web" -m tcp -j DNAT --to-destination 10.244.166.136:8081
```

查看endpoint

```shell
$ kubectl get ep
----------------
NAME         ENDPOINTS                                                    AGE
kubernetes   192.168.1.101:6443                                           7d3h
web          10.244.104.36:8081,10.244.166.134:8081,10.244.166.136:8081   60m
```

从上面看出，最终到达了对应的service匹配的endpoint中。

**2. 集群之外，通过浏览器访问，访问的是 <任意nodeIP>:nodePort，如这里的 192.168.1.101:30528**

自己跟踪一下试试

## 3. ipvs（更推荐）

![services-ipvs-overview.png](./assets/README-1628829470039.png)

- 首先由master节点中的ApiServer来控制kube-proxy，由kube-proxy监听Service规则匹配到的endpoint，动态地增加和移除endpoint。
- 当Service添加一个endpoint时，Service管理一组VirtualServer负载均衡规则，
- 浏览器中访问Service的ClusterIP，根据VirtualServer与RealServer的规则关系，把流量转发到后端的pod中

**值得一提的是**：ipvs模式，需要在运行kube-proxy之前，确保节点上已经ipvs内核模块。当kube-proxy以ipvs方式启动时，会先验证节点是否已经安装好ipvs模块，如果没有，则退化成iptables模式。

另外，kube-proxy是如何监听Service对于endpoint的变化呢？它是以watch方式监听着kube-apiserver写入到etcd中的pod的最新状态信息。一旦发生变化，马上反馈到 iptables / ipvs
规则当中。

**为什么更推荐ipvs模式？**

1. kube-proxy 通过 iptables 处理 Service 的过程，需要在宿主机上设置相当多的 iptables 规则，如果宿主机有大量的Pod，不断刷新iptables规则，会消耗大量的CPU资源。
2. iptables的规则是存在内核空间，但是iptables这个软件是工作在用户空间，规则生成并且存入到内核空间，也是存在用户空间与内核空间通信。

**实践5：使用ipvs代理模式暴露一个服务，并且跟踪service生成的ipvs记录**

确认是否开启ipvs内核模块

```shell
lsmod | grep ip_vs
```

没开启的话，开启方式

```shell
modprobe -- ip_vs
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_sh
modprobe -- nf_conntrack_ipv4
```

修改service的代理模式为ipvs

```shell
$ kubectl edit configmap kube-proxy -n kube-system
--------------------------------------------------
# 找到字段 mode，并修改为 "ipvs"
...
    mode: "ipvs"
    nodePortAddresses: null
...
```

重启kube-proxy

```shell
$ kubectl delete po -n kube-system $(kubectl get po -n kube-system | grep kube-proxy | awk '{print $1}')
```

验证是否已经使用ipvs, **注意 kube-proxy-8dr57 需要修改为你的实际pod id**

```shell
$ kubectl logs kube-proxy kube-proxy-8dr57 -n kube-system
--------------------------------------------------------
I0806 20:36:15.735810       1 node.go:136] Successfully retrieved node IP: 192.168.1.103
I0806 20:36:15.736587       1 server_others.go:111] kube-proxy node IP is an IPv4 address (192.168.1.103), assume IPv4 operation
I0806 20:36:15.862042       1 server_others.go:259] Using ipvs Proxier.
```

看到Using ipvs Proxier 说明已经启用了

安装ipvsadm工具

```shell
$ yum intall -y ipvsadm
```

**跟踪ipvs规则：集群之内，在node或者pod里，通过curl，访问ClusterIP:80端口，如这里的 curl 10.1.155.208:80**

查看ipvs规则

```shell
$ ipvsadm -L -n | grep -C "10.1.155.208"
----------------------------------------
IP Virtual Server version 1.2.1 (size=4096)
Prot LocalAddress:Port Scheduler Flags
  -> RemoteAddress:Port           Forward Weight ActiveConn InActConn
...
TCP  10.1.155.208:80 rr
  -> 10.244.104.36:8081           Masq    1      0          0
  -> 10.244.166.134:8081          Masq    1      0          0
  -> 10.244.166.136:8081          Masq    1      0          0
...
```

发现规则很简单，就是就是按照Weight权重1:1:1，转发到10.244.104.36:8081，10.244.166.134:8081 和 10.244.166.136:8081，这个是service匹配到的endpoint

那么谁来接收 10.1.155.208 的流量呢？

查看网卡

```shell
$ ip addr
---------------------------------
55: kube-ipvs0: <BROADCAST,NOARP> mtu 1500 qdisc noop state DOWN group default
    link/ether e2:6e:15:af:31:6c brd ff:ff:ff:ff:ff:ff
    inet 10.1.0.10/32 scope global kube-ipvs0
       valid_lft forever preferred_lft forever
    inet 10.1.159.83/32 scope global kube-ipvs0
       valid_lft forever preferred_lft forever
    inet 10.1.124.50/32 scope global kube-ipvs0
       valid_lft forever preferred_lft forever
    inet 10.1.0.1/32 scope global kube-ipvs0
       valid_lft forever preferred_lft forever
    inet 10.1.155.208/32 scope global kube-ipvs0
       valid_lft forever preferred_lft forever
```

kubernetes会创建一个kube-ipvs0的网卡，用于接受通过ClusterIP流量。

总结ipvs访问的流程： **curl service ip -> kube-ipvs0(virtual server) -> pod (Real Server)**

ipvs是内核的一个模块，具体的应用是LVS，更多信息，参考: [https://blog.csdn.net/xiaoaps/article/details/105140854](https://blog.csdn.net/xiaoaps/article/details/105140854)

# 五、CoreDNS的作用

![pod-service](./assets/README-1628838427398.png)

问题：两个不同的service代理的pod容器之间，如何相互访问？

![coredns](./assets/README-1628836973113.png)

CoreDNS是一个DNS服务，Kubernetes默认采用，以Pod部署在集群中，CoreDNS服务监视kubernetes API，为每一个Service创建DNS记录用于域名解析。

ClusterIP A记录格式：[service-name].[namespace-name].svc.cluster.local

比如 一个存在于default命名空间的service，名为 web，记录为：web.default.svc.cluster.local

**实践6：利用CoreDNS的service_name访问服务**

利用nslookup解析一下域名

busybox-dns.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: busybox
  name: busybox
spec:
  replicas: 1
  selector:
    matchLabels:
      app: busybox
  strategy: { }
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: busybox
    spec:
      containers:
        - image: busybox:1.28.4
          name: busybox
          command: [ 'sh','-c','echo The app is running! & sleep 3600' ]
          resources: { }
status: { }
---
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: busybox
  name: busybox
spec:
  ports:
    - port: 80
      protocol: TCP
      targetPort: 80
  selector:
    app: busybox
  type: NodePort
status:
  loadBalancer: { }
```

```shell
$ kubectl apply -f busybox-dns.yaml
```

查看po，svc

```shell
$ kubectl get po,svc 
-----------------------------
NAME                          READY   STATUS    RESTARTS   AGE
pod/busybox-b6bc75768-p8nbf   1/1     Running   0          2m31s

NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
service/busybox      NodePort    10.1.106.200   <none>        80:32631/TCP   2m31s
```

进入到容器中，执行nslookup解析域名 busybox.default.svc.cluster.local

```shell
$ kubectl exec -it busybox-b6bc75768-p8nbf -- sh
------------------------------------------------
# 在容器中执行
nslookup busybox.default.svc.cluster.local
Server:    10.1.0.10
Address 1: 10.1.0.10 kube-dns.kube-system.svc.cluster.local

Name:      busybox.default.svc.cluster.local
Address 1: 10.1.106.200 busybox.default.svc.cluster.local

# 在容器中执行，查看是否能够访问到web
nslookup web.default.svc.cluster.local
Server:    10.1.0.10
Address 1: 10.1.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web.default.svc.cluster.local
Address 1: 10.1.155.208 web.default.svc.cluster.local
```

可以看出来，解析到的ip就是service的ClusterIP

# 六、Ingress应用

**Ingress为弥补NodePort的不足**

- 一个端口只能一个服务使用，端口需提前规划
- 只支持4层负载均衡

## Ingress介绍

![ingress](./assets/README-1628840728201.png)

Ingress公开了从集群外部到集群内服务的HTTP和HTTPS路由的规则集合，而具体实现流量路，由则是由Ingress Controller负责。

**Ingress**：K8s中的一个抽象资源，给管理员 提供一个暴露应用的入口定义方法

**Ingress Controller**：根据Ingress生成具体 的路由规则，并对Pod负载均衡

使用流程：

1. 部署Ingress Controller
2. 创建Ingress规则

**实践7：使用Ingress做根据域名的流量分发**

下载ingress controller yaml文件

```shell
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml -o ingress-controller.yaml
```

修改yaml文件中的nginx-ingress-controller的镜像为 registry.cn-shenzhen.aliyuncs.com/jayzone/nginx-ingress-controller:0.30.0

（由于官方下载的yaml文件中nginx-ingress-controller的镜像可能拉不下来，所以我已经放到我的阿里云的镜像仓库中）

增加 hostNetwork: true 字段

```shell
...
    spec:
      hostNetwork: true # 此处很重要
      # wait up to five minutes for the drain of connections
      terminationGracePeriodSeconds: 300
      serviceAccountName: nginx-ingress-serviceaccoun
...
```

我已经放在了k8s-study/chapter04/yaml/里面，需要拉下来

```shell
$ git clone https://oauth2:tmsHZgob-AQNA9C7-BD5@git.mysre.cn/jintang/k8s-study.git
```

然按后安装ingress-controller

```shell
$ kubectl apply -f k8s-study/chapter04/yaml/ingress-controller.yaml
```

配置ingress规则, ingress-rule.yaml

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: minimal-ingress
spec:
  rules:
    - host: web.yunlian-k8s.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: web
                port:
                  number: 8081
    - host: kdemo.yunlian-k8s.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: kdemo
                port:
                  number: 80
```

应用规则

```shell
$ kubectl apply -f ingress-rule.yaml
```

查看ingress-controller所在的node

```shell
$ kubectl get po -n ingresss-nginx -o wide
-------------------------------------------
NAME                                        READY   STATUS    RESTARTS   AGE     IP              NODE    NOMINATED NODE   READINESS GATES
nginx-ingress-controller-5d7df9f986-bbgjz   1/1     Running   0          4m13s   192.168.1.102   node1   <none>           <none>
```

发现ingress-controller的pod在node1上

因为域名是自己随意写的，没有经过解析，我们采用修改本地的host方式，修改你本地电脑(虚拟机的宿主机)的host

```shell
$ vim /etc/hosts
---------------
192.168.1.102 web.yunlian-k8s.com
192.168.1.102 kdemo.yunlian-k8s.com
```

浏览器访问：http://web.yunlian-k8s.com http://kdemo.yunlian-k8s.com

# 七、作业

1. 以上实践都重新做一遍，尤其是iptables与ipvs的跟踪，结合实践深刻理解Service实现的负载均衡。
