# 02. kubernetes集群搭建实战

## 一、kubernetes核心架构

![核心架构](./assets/README-1628068358174.png)

- API server是所有请求的唯一入口
- api server管理所有的事务，并把信息记录到etcd数据库中
- etcd有一个自动服务发现的特性机制，etcd会搭建有三个节点的集群，实现三副本
- scheduler 调度器用来调度资源，查看业务节点的资源情况，确定在哪个node上创建pod，把指令告知给api server
- 控制管理器controller-manager管理pod，比如pod挂了，需要拉起
- pod可以分为有状态和无状态的pod，一个pod可以执行有多个容器，容器之间可以相互配合，比如SideCar边车
- api server 把任务下发给业务节点的kubelet去执行
- 客户访问通过kube-proxy去访问pod
- pod下面的容器不一定是docker，还有别的容器，比如containerd、podman

## 二、kubernetes集群搭建

### 1. master节点kubernetes环境安装

开启br_netfilter

```shell
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF
```

将桥接的IPV4流量传递到iptables(使用docker的话，不设置也没什么问题，因为dockerd会帮我们设置，但是如果使用containerd的话，就需要设置)

```shell
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
```

让sysctl生效

```shell
sysctl --system
```

配置k8s的阿里云源

```shell
cat >>/etc/yum.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```

安装kubelet、kubeadm、kubectl

```shell
yum install -y kubelet-1.19.3 kubeadm-1.19.3 kubectl-1.19.3
```

自动启动

```shell
systemctl enable kubelet
```

### 2. 创建node节点

首先修改 master 节点的配置，确保是2核2G以上的配置

![WX20210804-112227.png](./assets/README-1628047388404.png)

配置处理器和内存，注意需要在关机状态下才可以设置

![配置处理器和内存](./assets/README-1628047434523.png)

选择2核2G

![选择2核2G](./assets/README-1628047489832.png)

制作 Snapshot 镜像快照，后续 worker node 的初始化可以基于这个 snapshot进行安装

![快照](./assets/README-1628047704297.png)

![拍摄](./assets/README-1628047725593.png)

![命名](./assets/README-1628047787011.png)

重命名master节点为base

![重命名base](./assets/README-1628059213687.png)

然后分别在base基础上，克隆出 master，node1 和 node2 三台主机。注意使用链接克隆，原因是链接克隆比普通克隆更加节省磁盘空间，但是克隆时，每次都从base进行克隆。

![链接克隆base](./assets/README-1628059362196.png)

存储为 node1

![链接克隆保存为node1](./assets/README-1628048571747.png)

集群需要的三个节点创建好了：master，node1，node2

![集群](./assets/README-1628059783598.png)

下面以 node1 为例进行配置，node2 照搬就行。

先启动node1，然后直接在虚拟机窗口，配置内网地址为192.168.1.102，注意修改的网卡配置文件为**ifcfg-ens37**

```shell
cd /etc/sysconfig/network-scripts/
# 替换掉192.168.1.101为192.168.1.102
sed -i "s/192.168.1.101/192.168.102/g" ifcfg-ens37
```

重启网络

```shell
systemctl restart network
```

在宿主机上，添加node1的ssh登录

```shell
cat >> ~/.ssh/config <<EOF
Host node1
    Hostname 192.168.1.102
    Port 22
    User root
EOF
```

然后在宿主机上，使用 iterm2 登录 node1

```shell
ssh node1
```

至此，node1 节点创建完成。node2 重复上述方法

### 3. 集群搭建

#### 设置master节点

在 master 上，设置 hostname

```shell
hostnamectl set-hostname master
```

确保 /etc/hosts 一直配置好

```shell
cat /etc/hosts
# 可以看到
192.168.1.101 master
192.168.1.102 node1
192.168.1.103 node2
```

使用 kubeadm 初始化 master 节点

```shell
kubeadm init --kubernetes-version=1.19.3 \
  --apiserver-advertise-address=192.168.1.101 \
  --image-repository registry.aliyuncs.com/google_containers \
  --service-cidr=10.1.0.0/16 \
  --pod-network-cidr=10.244.0.0/16
```

注意：--kubernetes-version 为上面安装时的 kubernetes 版本，--apiserver-advertise-address 为 master 节点的 ip 地址

看到successful! 说明已经成功了

![success](./assets/README-1628061100318.png)

然后你会看到生成了一段命令，kebeadm join xxx，这是给node节点加入集群使用的。

配置环境

```shell
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

配置kubeconfig环境变量

```shell
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >>~/.bash_profile
```

验证一下，看到以下信息，说明配置已经生效，但是当前集群的master节点仍未生效，处于**NotReady**状态，原因是我们还没有安装网络组件

```shell
[root@master ~]# kubectl get node
NAME     STATUS     ROLES    AGE   VERSION
master   NotReady   master   17m   v1.19.3
```

安装calico网络组件

```shell
cd ~
git clone https://oauth2:tmsHZgob-AQNA9C7-BD5@git.mysre.cn/jintang/k8s-study.git
kubectl apply -f k8s-study/chapter02/yaml/calico.yaml
```

其实calico.yaml也可以直接calico官网下载，由于可能需要翻墙，所以这里我先下好

再次查看节点

```shell
[root@master ~]# kubectl get node
NAME     STATUS     ROLES    AGE   VERSION
master   Ready   master   17m   v1.19.3
```

查看一下kubernetes都运行了那些pod

```shell
kubectl get pod -n kube-system
#
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-7f4f5bf95d-nvntk   1/1     Running   0          9h
calico-node-g25dl                          1/1     Running   0          5h25m
coredns-6d56c8448f-v9mrf                   1/1     Running   0          10h
coredns-6d56c8448f-w5gqx                   1/1     Running   0          10h
etcd-master                                1/1     Running   0          10h
kube-apiserver-master                      1/1     Running   0          10h
kube-controller-manager-master             1/1     Running   0          10h
kube-proxy-vfkqh                           1/1     Running   0          10h
kube-scheduler-master                      1/1     Running   0          10h
```

加上参数 -o 能够查看更多信息

至此，我们集群的master节点已经搭建成功了。

#### node节点加入集群

设置hostname

```shell
hostnamectl set-hostname node1
```

下面开始把node节点加入到集群，以node1为例

拷贝kubeadm join加入集群命令：

```shell
kubeadm join 192.168.1.101:6443 \
  --token 8fjy4g.bln0xvb0w66vjh70 \
  --discovery-token-ca-cert-hash \
  sha256:8054ee2b94864ead0e15fc99ce894d2304bef6f5d4ca922b8a889c1c967a5c8e
```

注意别拷贝我这一段，你需要拷贝你在master节点初始化后生成的那一段

如果忘记记录没关系，可以在 master 节点上执行以下命令的到

```shell
kubeadm token create --print-join-command
```

拷贝kubeadm join，在node1上执行

```shell
kubeadm join 192.168.1.101:6443 \
  --token 8fjy4g.bln0xvb0w66vjh70 \
  --discovery-token-ca-cert-hash \
  sha256:8054ee2b94864ead0e15fc99ce894d2304bef6f5d4ca922b8a889c1c967a5c8e
```

为了让node1,node2也能使用kubectl命令，需要配置一下

```shell
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/kubelet.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

加入集群后，可以在 master 再次查看节点

```shell
[root@master ~]# kubectl get node
NAME     STATUS   ROLES    AGE   VERSION
master   Ready    master   46m   v1.19.3
node1    Ready    <none>   87s   v1.19.3
```

说明集群已经搭建好了，用同样的方法，把node2也加进来

再次看看 Kubernetes 运行了哪些 pod

```shell
kubectl get pod -n kube-system
#
NAME                                       READY   STATUS    RESTARTS   AGE     IP              NODE     NOMINATED NODE   READINESS GATES
calico-kube-controllers-7f4f5bf95d-nvntk   1/1     Running   0          9h      10.244.219.67   master   <none>           <none>
calico-node-g25dl                          1/1     Running   0          5h25m   192.168.1.103   node2    <none>           <none>
calico-node-g567x                          1/1     Running   0          9h      192.168.1.102   node1    <none>           <none>
calico-node-wz24v                          1/1     Running   0          9h      192.168.1.101   master   <none>           <none>
coredns-6d56c8448f-v9mrf                   1/1     Running   0          10h     10.244.219.65   master   <none>           <none>
coredns-6d56c8448f-w5gqx                   1/1     Running   0          10h     10.244.219.66   master   <none>           <none>
etcd-master                                1/1     Running   0          10h     192.168.1.101   master   <none>           <none>
kube-apiserver-master                      1/1     Running   0          10h     192.168.1.101   master   <none>           <none>
kube-controller-manager-master             1/1     Running   0          10h     192.168.1.101   master   <none>           <none>
kube-proxy-2w5rc                           1/1     Running   0          9h      192.168.1.102   node1    <none>           <none>
kube-proxy-7mfph                           1/1     Running   0          5h25m   192.168.1.103   node2    <none>           <none>
kube-proxy-vfkqh                           1/1     Running   0          10h     192.168.1.101   master   <none>           <none>
kube-scheduler-master                      1/1     Running   0          10h     192.168.1.101   master   <none>           <none>
```

注意看看 pod 在各个节点的分布

## 三、kubernetes集群初始化、CNI及配置文件讲解

### 1. 初始化过程

看看kubeadm为我们初始化集群都做了什么？

![初始化过程](./assets/README-1628060994783.png)

1. **[prefight]** 一系列检查工作，已确定这台机器可以用来部署Kubernetes
2. **[certs]** 生成Kubernetes对外提供服务所需要的各种证书和对应目录
3. **[kubeconfig]** 为其他组件访问生成kube-apiserver所需要的配置文件
4. **[control-plane]** 为Master组件生成Pod配置文件(Static Pod, 部署的Pod的YAML文件放到指定的目录里, 当启动kubelet时，自动检查这个目录，加载YAML文件)
    - kube-apiserver
    - kube-controller-manager
    - kube-scheduler
5. **[kubelet]** 将ca.crt等Master节点的重要信息，通过ConfigMap的方式保存子Etcd当中，供后续部署Node节点使用
6. **[bootstrap-token]** 为集群生成一个bootstrap token
7. **[addons]** 安装默认插件(CoreDNS，kube-proxy)

### 2. kubeconfig文件作用

![kubeconfig](./assets/README-1628127233185.png)

kubeconfig文件好比一把钥匙，它里面配置了具体的集群地址、权限用户信息和相关证书。他是由kubeadm在 init 的时候生成，通过 join 来拷贝给 node 节点。

指定kubeconfig的方式有三种：

**.kube/config**

```shell
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/kubelet.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

注意，如果是node节点，需要在join进群以后，才会在/etc/kubernetes下生成kubelet.conf文件

**设置环境变量 $KUBECONFIG**

```shell
echo "export KUBECONFIG=/etc/kubernetes/kubelet.conf" >> ~/.bash_profile
source ~/.bash_profile
```

**使用参数 --kubeconfig 指定**

```shell
kubectl get node --kubeconfig=/etc/kubernetes/kubelet.conf
```

### 3. 网络模型CNI与Calico组件作用

**问：为什么我们在集群搭建的时候，需要安装 calico 网络组件？**

![ca](./assets/README-1628129024244.png)

需要解决的问题：

1. 两个节点的容器内部ip分配是一样的，怎么处理？
2. 容器1访问容器2，容器1如何知道容器2的docker主机的ip？

你能想到的解决办法是什么？

网络组件的作用，必须实现CNI网络模型接口的如下功能：

1. 一个Pod分配一个唯一的IP
2. 所有的Pod可以与任何语言的Pod直接通信
3. 所有节点可以与所有Pod直接通信
4. Pod内部获取到的ip地址与其他Pod或者其他节点与其通信时的ip地址是同一个

目前主流的网络组件：flannel、calico等

### 4. 为什么kubernetes要弃用docker？

docker 比 kubernetes 要早出现，kubernetes 想要使用 docker 作为它的容器技术，必须要解决与容器运行时（containerd）集成的问题，所以早期 kubernetes 社区推出 CRI
（Container Runtime Interface, 容器运行时接口），同时也能在将来让其他的容器结束接进来。目前 kubernetes 在使用 docker 作为容器技术，架构是这样的：

![cri](./assets/README-1628130043617.png)

docker是老大，不支持kubernetes的CRI，所以Kubernetes实现了一个dockershim插件来与dockerd通信，现在kubernetes已经硬气了，他可以不在更新维护dockershim，甚至在后序某个版本直接移除掉。后序他可以直接通过使用支持CRI的容器技术，如podman，containerd等。

但是大家别觉得kubernetes启用docker就觉得docker没价值，其实依然值得学习，他们所在的战场不同。

## 四、kubernetes命令介绍

### 1. kubectl工具

首先设置kubectl的别名kc，使用便捷命令kc

```shell
alias kc=kubectl
echo "alias kc=kubectl" > ~/.bash_profile
```

kubectl工具的概述，可以参考 [官网地址](https://kubernetes.io/zh/docs/reference/kubectl/overview/)
, [备忘单](https://kubernetes.io/zh/docs/reference/kubectl/cheatsheet/)

下面讲解一些常用的, 使用 kubectl help 查看

kubectl 命令补全工具

```shell
yum install -y bash-completion
source /usr/share/bash-completion/bash_completion
source <(kubectl completion bash)
```

然后敲 kubectl app + tab，就能自动补全或者提示

```shell
[root@master ~]# kubectl ap
api-resources  api-versions   apply
```

### 2. yaml文件介绍

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web
  strategy: { }
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
        - image: nginx
          name: nginx
          resources: { }
status: { }
```

使用 yaml 声明文件来定义

```shell
kubectl apply -f xxx.yaml
kubectl delete -f xxx.yaml
```

获取某一个正在运行的 po 的yaml文件

```shell
kubectl get pod kubectl get pod web-96d5df5c8-pcsng -o yaml
```

获取某种资源的 yaml 声明文件

```shell
kubectl create deployment web --image=nginx --dry-run=client -o yaml
```

## 五、实践

### 实践1：暴露一个nginx服务，并且使用常用命令进行操作实践

拉取nginx镜像

```shell
docker pull nginx
```

部署nginx

```shell
kubectl create deployment web --image=nginx
```

暴露nginx服务

```shell
kubectl expose deployment web --port=80 --type=NodePort --target-port=80 --name=web
```

查看暴露的随机端口

```shell
kubectl get service

AME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE   SELECTOR
kubernetes   ClusterIP   10.1.0.1      <none>        443/TCP        8h    <none>
web          NodePort    10.1.142.87   <none>        80:32448/TCP   79s   app=web
```

发现是**32448**, 所以可以通过浏览器访问任意的节点IP + 32448来访问，如 node1: https://192.168.1.102:32448

查看 nginx 运行的 pod 和 service

```shell
kubectl get pod,

NAME                      READY   STATUS    RESTARTS   AGE
pod/web-96d5df5c8-pcsng   1/1     Running   0          57m

NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
service/kubernetes   ClusterIP   10.1.0.1       <none>        443/TCP        9h
service/web          NodePort    10.1.142.87    <none>        80:32448/TCP   9m
```

进入 nginx 容器，把/etc/nginx/conf.d/default.conf的listen 80端口改成8080

```shell
kubectl exec -it web-96d5df5c8-pcsng -- bash
# 在容器里运行
sed -i 's/80/8080/g' /etc/nginx/conf.d/default.conf
nginx -s reload
```

此时访问 https://192.168.1.102:32448 发现已经访问不了了

修改 service 的 targetPort

```shell
kubectl edit service web
# 修改targetPort的值为8080
```

再次访问 https://192.168.1.102:32448 又好了

### 实践2：kubernetes-dashboard安装

通过 yaml 文件安装

```shell
kubectl apply -f k8s-study/chapter02/yaml/kubernetes-dashboard.yaml
```

通过查看 kubernetes-dashboard

```shell
kubectl get pods -n kubernetes-dashboard
```

![getpod](./assets/README-1628083143627.png)

当都是running时，说明已经生效了

查看 kubernetes-dashboard 的服务端口

```shell
kubectl get services -n kubernetes-dashboard
```

![30001](./assets/README-1628083253285.png)

发现端口是 **30001**，此时，我们可以通过浏览器访问任意节点的ip地址+端口30001来访问 kubernetes-dashboard

如 node1 ：浏览器访问地址：[https://192.168.1.102:30001](https://192.168.1.102:30001)，注意一定是https

打开发现浏览器有安全警告

![安全警告](./assets/README-1628083443384.png)

解决办法：鼠标点击当前页面，直接键盘输入‘thisisunsafe’，然后回车，即可进行页面跳转至 Dashboard登录页

![dashboard](./assets/README-1628083548079.png)

获取登录 token

```shell
kubectl -n kube-system describe $(kubectl -n kube-system get secret -n kube-system -o name | grep namespace) | grep token
```

然后拷贝 token 输入后，点击登录即可

### 实践3：部署kdemo

还要我教你吗？

## 六、思考

为什么在部署完 kubernetes-dashboard 后，在使用任意节点的ip都能访问？

## 七、作业

1. 完成以上实操演练，成功搭建 1个master 和 2个node 的集群环境，并且安装好 kubernetes-dashboard
2. 结合集群搭建过程（master初始化，node节点加入集群等），理解核心架构、CNI等原理，把你的理解心得分享出来
3. 成功部署kdemo，About页面发现数据库连接不上，没有部署数据库？docker部署的那个能用吗？

作业以截图和文字的形式发群里。