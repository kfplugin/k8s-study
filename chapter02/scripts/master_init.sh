#!/bin/bash

# 设置主机名为master
hostnamectl set-hostname master

# 初始化主节点
kubeadm init --kubernetes-version=1.19.3 \
  --apiserver-advertise-address=192.168.1.101 \
  --image-repository registry.aliyuncs.com/google_containers \
  --service-cidr=10.1.0.0/16 \
  --pod-network-cidr=10.244.0.0/16

# 配置环境
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# 配置kubeconfig环境变量
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >>~/.bash_profile

# 启动flannel插件
kubectl apply -f yaml/calico.yaml
